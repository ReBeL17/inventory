<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admins = [
            [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
                'remember_token' => null,
            ],
            [
                'name' => 'IT Admin',
                'email' => 'itadmin@admin.com',
                'password' => bcrypt('password'),
                'remember_token' => null,
            ],
        ];
        Admin::insert($admins);
    }
}
