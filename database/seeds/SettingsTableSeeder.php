<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $settings = [
            [
                'title' => 'Inventory Sys',
                'logo' => null,
                'favicon' => null,
                'copyright' => null,
                'footer_logo' => null
            ],
        ];

        Setting::insert($settings);
    }
}
