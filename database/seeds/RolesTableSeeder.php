<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'title' => 'Admin',
                'slug' => 'admin'
            ],
            [
                'title' => 'User',
                'slug' => 'user'
            ],
            [
                'title' => 'IT Admin',
                'slug' => 'IT-admin'
            ],
        ];

        Role::insert($roles);
    }
}
