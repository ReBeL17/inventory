<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            [
                
                'title' => 'user_management_access',
                'slug' => 'user-management-access',
            ],
            [
                
                'title' => 'permission_create',
                'slug' => 'permission-create',
            ],
            [
                
                'title' => 'permission_edit',
                'slug' => 'permission-edit',
            ],
            [
                
                'title' => 'permission_show',
                'slug' => 'permission-show',
            ],
            [
                
                'title' => 'permission_delete',
                'slug' => 'permission-delete',
            ],
            [
                
                'title' => 'permission_access',
                'slug' => 'permission-access',
            ],
            [
                
                'title' => 'role_create',
                'slug' => 'role-create',
            ],
            [
                
                'title' => 'role_edit',
                'slug' => 'role-edit',
            ],
            [
                
                'title' => 'role_show',
                'slug' => 'role-show',
            ],
            [
                
                'title' => 'role_delete',
                'slug' => 'role-delete',
            ],
            [
                
                'title' => 'role_access',
                'slug' => 'role-access',
            ],
            [
                
                'title' => 'user_create',
                'slug' => 'user-create',
            ],
            [
                
                'title' => 'user_edit',
                'slug' => 'user-edit',
            ],
            [
                
                'title' => 'user_show',
                'slug' => 'user-show',
            ],
            [
                
                'title' => 'user_delete',
                'slug' => 'user-delete',
            ],
            [
                
                'title' => 'user_access',
                'slug' => 'user-access',
            ],
            [
                
                'title' => 'group_create',
                'slug' => 'group-create',
            ],
            [
                
                'title' => 'group_edit',
                'slug' => 'group-edit',
            ],
            [
                
                'title' => 'group_show',
                'slug' => 'group-show',
            ],
            [
                
                'title' => 'group_delete',
                'slug' => 'group-delete',
            ],
            [
                
                'title' => 'group_access',
                'slug' => 'group-access',
            ],
            [
                
                'title' => 'supplier_management_access',
                'slug' => 'supplier-management-access',
            ],
            [
                
                'title' => 'suppliertype_create',
                'slug' => 'suppliertype-create',
            ],
            [
                
                'title' => 'suppliertype_edit',
                'slug' => 'suppliertype-edit',
            ],
            [
                
                'title' => 'suppliertype_show',
                'slug' => 'suppliertype-show',
            ],
            [
                
                'title' => 'suppliertype_delete',
                'slug' => 'suppliertype-delete',
            ],
            [
                
                'title' => 'suppliertype_access',
                'slug' => 'suppliertype-access',
            ],
            [
                
                'title' => 'suppliers_create',
                'slug' => 'suppliers-create',
            ],
            [
                
                'title' => 'suppliers_edit',
                'slug' => 'suppliers-edit',
            ],
            [
                
                'title' => 'suppliers_show',
                'slug' => 'suppliers-show',
            ],
            [
                
                'title' => 'suppliers_delete',
                'slug' => 'suppliers-delete',
            ],
            [
                
                'title' => 'suppliers_access',
                'slug' => 'suppliers-access',
            ],
            [
                
                'title' => 'product_management_access',
                'slug' => 'product-management-access',
            ],
            [
                
                'title' => 'category_create',
                'slug' => 'category-create',
            ],
            [
                
                'title' => 'category_edit',
                'slug' => 'category-edit',
            ],
            [
                
                'title' => 'category_show',
                'slug' => 'category-show',
            ],
            [
                
                'title' => 'category_delete',
                'slug' => 'category-delete',
            ],
            [
                
                'title' => 'category_access',
                'slug' => 'category-access',
            ],
            [
                
                'title' => 'product_create',
                'slug' => 'product-create',
            ],
            [
                
                'title' => 'product_edit',
                'slug' => 'product-edit',
            ],
            [
                
                'title' => 'product_show',
                'slug' => 'product-show',
            ],
            [
                
                'title' => 'product_delete',
                'slug' => 'product-delete',
            ],
            [
                
                'title' => 'product_access',
                'slug' => 'product-access',
            ],
            [
                
                'title' => 'purchase_management_access',
                'slug' => 'purchase-management-access',
            ],
            [
                
                'title' => 'purchase_create',
                'slug' => 'purchase-create',
            ],
            [
                
                'title' => 'purchase_edit',
                'slug' => 'purchase-edit',
            ],
            [
                
                'title' => 'purchase_show',
                'slug' => 'purchase-show',
            ],
            [
                
                'title' => 'purchase_delete',
                'slug' => 'purchase-delete',
            ],
            [
                
                'title' => 'purchase_access',
                'slug' => 'purchase-access',
            ],
            [
                
                'title' => 'customer_create',
                'slug' => 'customer-create',
            ],
            [
                
                'title' => 'customer_edit',
                'slug' => 'customer-edit',
            ],
            [
                
                'title' => 'customer_show',
                'slug' => 'customer-show',
            ],
            [
                
                'title' => 'customer_delete',
                'slug' => 'customer-delete',
            ],
            [
                
                'title' => 'customer_access',
                'slug' => 'customer-access',
            ],
             [
                
                'title' => 'sale_management_access',
                'slug' => 'sale-management-access',
            ],
            [
                
                'title' => 'sale_create',
                'slug' => 'sale-create',
            ],
            [
                
                'title' => 'sale_edit',
                'slug' => 'sale-edit',
            ],
            [
                
                'title' => 'sale_show',
                'slug' => 'sale-show',
            ],
            [
                
                'title' => 'sale_delete',
                'slug' => 'sale-delete',
            ],
            [
                
                'title' => 'sale_access',
                'slug' => 'sale-access',
            ],
            [
                
                'title' => 'payment_create',
                'slug' => 'payment-create',
            ],
            [
                
                'title' => 'payment_edit',
                'slug' => 'payment-edit',
            ],
            [
                
                'title' => 'payment_show',
                'slug' => 'payment-show',
            ],
            [
                
                'title' => 'payment_delete',
                'slug' => 'payment-delete',
            ],
            [
                
                'title' => 'payment_access',
                'slug' => 'payment-access',
            ],
            [
                
                'title' => 'receipt_print',
                'slug' => 'receipt-print',
            ],
            [
                
                'title' => 'receipt_access',
                'slug' => 'receipt-access',
            ],
            [
                
                'title' => 'receive_create',
                'slug' => 'receive-create',
            ],
            [
                
                'title' => 'receive_edit',
                'slug' => 'receive-edit',
            ],
            [
                
                'title' => 'receive_show',
                'slug' => 'receive-show',
            ],
            [
                
                'title' => 'receive_delete',
                'slug' => 'receive-delete',
            ],
            [
                
                'title' => 'receive_access',
                'slug' => 'receive-access',
            ],
            [
                
                'title' => 'invoice_print',
                'slug' => 'invoice-print',
            ],
            [
                
                'title' => 'invoice_access',
                'slug' => 'invoice-access',
            ],
            [
                
                'title' => 'setting_create',
                'slug' => 'setting-create',
            ],
        ];

        Permission::insert($permissions);
    }
}
