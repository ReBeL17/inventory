<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'Admin\HomeController@index')->name('dashboard');
// Route::get('/drilldown', 'Admin\HomeController@drilldown')->name('dash');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login');
    Route::get('/', 'Admin\HomeController@index')->name('dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('logout');

    //change password
    Route::get('change-password', 'Admin\ChangePasswordController@create')->name('password.create');
    Route::post('change-password', 'Admin\ChangePasswordController@update')->name('password.update');

    //password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('password.reset');

    // Permissions
    Route::delete('permissions/destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'Admin\PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'Admin\RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'Admin\RolesController');

    // Users
    Route::delete('users/destroy', 'Admin\UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'Admin\UsersController');

    // groups
    Route::delete('groups/destroy', 'Admin\GroupsController@massDestroy')->name('groups.massDestroy');
    Route::resource('groups', 'Admin\GroupsController');
    
    // settings
    Route::resource('settings', 'Admin\SettingsController');
    // Route::group(['namespace' => 'Inventory'], function () {
        
        includeRouteFiles(__DIR__ . '/UserBackend/');
    // });
});