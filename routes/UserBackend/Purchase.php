<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('purchases' , 'PurchaseController@index')->name('purchases');
        Route::get('purchases/create' , 'PurchaseController@create')->name('purchases.create');
        Route::post('purchases/store' , 'PurchaseController@store')->name('purchases.store');
        Route::get('purchases/{id}/edit' , 'PurchaseController@edit')->name('purchases.edit');
        Route::get('purchases/{id}/delete' , 'PurchaseController@delete')->name('purchases.delete');
        Route::patch('purchases/{id}' , 'PurchaseController@update')->name('purchases.update');
        Route::delete('purchases/destroy', 'PurchaseController@massDestroy')->name('purchases.massDestroy');
    });
