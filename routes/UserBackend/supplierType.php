<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('suppliers_type' , 'SupplierTypeController@index')->name('suppliers_type');
        Route::get('suppliers_type/create' , 'SupplierTypeController@create')->name('suppliers_type.create');
        Route::post('suppliers_type/store' , 'SupplierTypeController@store')->name('suppliers_type.store');
        Route::get('suppliers_type/{id}/edit' , 'SupplierTypeController@edit')->name('suppliers_type.edit');
        Route::get('suppliers_type/{id}/delete' , 'SupplierTypeController@delete')->name('suppliers_type.delete');
        Route::patch('suppliers_type/{id}' , 'SupplierTypeController@update')->name('suppliers_type.update');
        Route::post('suppliers_type/createtype' , 'SupplierTypeController@createType')->name('suppliers_type.createtype');
        Route::delete('suppliers_type/destroy', 'SupplierTypeController@massDestroy')->name('suppliers_type.massDestroy');
    });
