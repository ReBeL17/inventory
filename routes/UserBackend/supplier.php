<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('suppliers' , 'SupplierController@index')->name('suppliers');
        Route::get('suppliers/create' , 'SupplierController@create')->name('suppliers.create');
        Route::post('suppliers/store' , 'SupplierController@store')->name('suppliers.store');
        Route::get('suppliers/{id}/edit' , 'SupplierController@edit')->name('suppliers.edit');
        Route::get('suppliers/{id}/delete' , 'SupplierController@delete')->name('suppliers.delete');
        Route::patch('suppliers/{id}' , 'SupplierController@update')->name('suppliers.update');
        Route::delete('suppliers/destroy', 'SupplierController@massDestroy')->name('suppliers.massDestroy');
    });
