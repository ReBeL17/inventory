<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('products' , 'ProductController@index')->name('products');
        Route::get('products/create' , 'ProductController@create')->name('products.create');
        Route::post('products/store' , 'ProductController@store')->name('products.store');
        Route::get('products/{id}/edit' , 'ProductController@edit')->name('products.edit');
        Route::get('products/{id}/delete' , 'ProductController@delete')->name('products.delete');
        Route::patch('products/{id}' , 'ProductController@update')->name('products.update');
        Route::get('products/getspecproducts' , 'ProductController@getSpecProducts')->name('products.getspecproducts');
        Route::get('products/getavailqty' , 'ProductController@getAvailQty')->name('products.getavailqty');
        Route::delete('products/destroy', 'ProductController@massDestroy')->name('products.massDestroy');
    });
