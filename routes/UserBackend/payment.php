<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('payments' , 'PaymentController@index')->name('payments');
        // Route::get('payments/create' , 'PaymentController@create')->name('payments.create');
        Route::post('payments/store' , 'PaymentController@store')->name('payments.store');
        // Route::get('payments/{id}/edit' , 'PaymentController@edit')->name('payments.edit');
        // Route::get('payments/{id}/delete' , 'PaymentController@delete')->name('payments.delete');
        // Route::patch('payments/{id}' , 'PaymentController@update')->name('payments.update');
        Route::get('payments/paymentdetails/{id}' , 'PaymentController@paymentDetails')->name('payments.paymentdetails');

    });
