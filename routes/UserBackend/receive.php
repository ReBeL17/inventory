<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('receives' , 'ReceiveController@index')->name('receives');
        // Route::get('receives/create' , 'ReceiveController@create')->name('receives.create');
        Route::post('receives/store' , 'ReceiveController@store')->name('receives.store');
        // Route::get('receives/{id}/edit' , 'ReceiveController@edit')->name('receives.edit');
        // Route::get('receives/{id}/delete' , 'ReceiveController@delete')->name('receives.delete');
        // Route::patch('receives/{id}' , 'ReceiveController@update')->name('receives.update');
        Route::get('receives/receivedetails/{id}' , 'ReceiveController@receiveDetails')->name('receives.receivedetails');

    });
