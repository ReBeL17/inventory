<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('invoices', 'InvoiceController@allInvoices')->name('invoices.allInvoices');
        Route::get('invoices/{receipt}', 'InvoiceController@generateInvoice')->name('invoices.generateInvoice');
        Route::get('invoices/{receipt}/print', 'InvoiceController@printInvoice')->name('invoice-print');
    });