<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('receipts', 'ReceiptController@allReceipts')->name('receipts.allReceipts');
        Route::get('receipts/{receipt}', 'ReceiptController@generateReceipt')->name('receipts.generateReceipt');
        Route::get('receipts/{receipt}/print', 'ReceiptController@printReceipt')->name('receipt-print');
    });