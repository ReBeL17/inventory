<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('brands' , 'BrandController@index')->name('brands');
        Route::get('brands/create' , 'BrandController@create')->name('brands.create');
        Route::post('brands/store' , 'BrandController@store')->name('brands.store');
        Route::get('brands/{id}/edit' , 'BrandController@edit')->name('brands.edit');
        Route::get('brands/{id}/delete' , 'BrandController@delete')->name('brands.delete');
        Route::patch('brands/{id}' , 'BrandController@update')->name('brands.update');
    });
