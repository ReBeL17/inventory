<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('sales' , 'SaleController@index')->name('sales');
        Route::get('sales/create' , 'SaleController@create')->name('sales.create');
        Route::post('sales/store' , 'SaleController@store')->name('sales.store');
        Route::get('sales/{id}/edit' , 'SaleController@edit')->name('sales.edit');
        Route::get('sales/{id}/delete' , 'SaleController@delete')->name('sales.delete');
        Route::patch('sales/{id}' , 'SaleController@update')->name('sales.update');
        Route::delete('sales/destroy', 'SaleController@massDestroy')->name('sales.massDestroy');
    });
