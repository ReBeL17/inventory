<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('customers' , 'CustomerController@index')->name('customers');
        Route::get('customers/create' , 'CustomerController@create')->name('customers.create');
        Route::post('customers/store' , 'CustomerController@store')->name('customers.store');
        Route::get('customers/{id}/edit' , 'CustomerController@edit')->name('customers.edit');
        Route::get('customers/{id}/delete' , 'CustomerController@delete')->name('customers.delete');
        Route::patch('customers/{id}' , 'CustomerController@update')->name('customers.update');
        Route::delete('customers/destroy', 'CustomerController@massDestroy')->name('customers.massDestroy');
    });
