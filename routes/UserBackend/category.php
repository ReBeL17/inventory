<?php
    Route::group(['namespace'  => 'Inventory'], function () {
        Route::get('categories' , 'CategoryController@index')->name('categories');
        Route::get('categories/create' , 'CategoryController@create')->name('categories.create');
        Route::post('categories/store' , 'CategoryController@store')->name('categories.store');
        Route::get('categories/{id}/edit' , 'CategoryController@edit')->name('categories.edit');
        Route::patch('categories/{id}' , 'CategoryController@update')->name('categories.update');
        Route::get('categories/{id}/delete' , 'CategoryController@delete')->name('categories.delete');
        Route::delete('categories/destroy', 'CategoryController@massDestroy')->name('categories.massDestroy');
    });
