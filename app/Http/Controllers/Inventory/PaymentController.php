<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Payment;
use App\Models\Inventory\Purchase;
use Auth;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class PaymentController extends Controller
{
    private $model, $purchase;

        
        public function __construct(Payment $model, Purchase $purchase)
        {
            $this->middleware('auth:admin');
            $this->model = $model;
            $this->purchase = $purchase;
           
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('payment-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $id=Auth::user()->id;
        $data = $this->purchase->where('admin_id',$id)->orderBy('status','asc')->paginate(40);
        // dd ($data);
        return view('userbackend.Inventory.Payment.index' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('userbackend.Inventory.Payment.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        $data['paid'] = $request->data['newpaid'];
        $data['supplier_id'] = $request->data['supplierID'];
        $data['purchaseID'] = $request->data['id'];
        // if($data['remarks']!="")
        $data['remarks'] = $request->data['remarks'];
        $data['user_id'] = Auth::user()->id;
        $total_paid = $request->data['oldpaid']+$data['paid'];
        // dd($request->data['total']);
        $latest=$this->model->create($data);
            // return $latest;
        // $data->save();

        // $supplyerB = Supplyer::find($data['supplyersID']);

        // $supplyer = Supplyer::where('id', $data['supplyersID'])
        //     ->update(['paid' => $supplyerB->paid+ $data['amount']]);


        if($request->data['total']==$total_paid) {
            $purchase = $this->purchase->where('purchaseID',$request->data['purchaseID'])->first();
            // dd($purchase);
            $dataPurchase = array();
            $dataPurchase['status'] = 2;
            $dataPurchase['due'] = $purchase->due - $data['paid'];

            $purchase->update($dataPurchase);
        }
        else{
            $purchase = $this->purchase->where('purchaseID',$request->data['purchaseID'])->first();
            $dataPurchase = array();
            $dataPurchase['status'] = 1;
            $dataPurchase['due'] = $purchase->due - $data['paid'];
            // dd($dataPurchase);


            $purchase->update($dataPurchase);
        } 

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        

    }

    public function paymentDetails($id)
    {
       $paymentdetails = $this->model->where('purchaseID',$id)->get(); 
    //    dd($paymentdetails);
        return $paymentdetails;
    }

}
