<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Receive;
use App\Models\Inventory\Sale;
use Auth;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class ReceiveController extends Controller
{
    private $model, $sale;

        
        public function __construct(Receive $model, Sale $sale)
        {
            $this->middleware('auth:admin');
            $this->model = $model;
            $this->sale = $sale;
           
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('receive-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $id=Auth::user()->id;
        $data = $this->sale->where('admin_id',$id)->orderBy('status','asc')->paginate(40);
        // dd ($data);
        return view('userbackend.Inventory.Receive.index' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('userbackend.Inventory.Payment.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        $data['paid'] = $request->data['newpaid'];
        $data['customer_id'] = $request->data['customerID'];
        $data['saleID'] = $request->data['id'];
        // if($data['remarks']!="")
        $data['remarks'] = $request->data['remarks'];
        $data['user_id'] = Auth::user()->id;
        $total_paid = $request->data['oldpaid']+$data['paid'];
        // dd($request->data['total']);
        $latest=$this->model->create($data);
            // return $latest;
        // $data->save();

        // $supplyerB = Supplyer::find($data['supplyersID']);

        // $supplyer = Supplyer::where('id', $data['supplyersID'])
        //     ->update(['paid' => $supplyerB->paid+ $data['amount']]);


        if($request->data['total']==$total_paid) {
            $sale = $this->sale->where('saleID',$request->data['saleID'])->first();
            // dd($purchase);
            $dataSale = array();
            $dataSale['status'] = 2;
            $dataSale['due'] = $sale->due - $data['paid'];

            $sale->update($dataSale);
        }
        else{
            $sale = $this->sale->where('saleID',$request->data['saleID'])->first();
            $dataSale = array();
            $dataSale['status'] = 1;
            $dataSale['due'] = $sale->due - $data['paid'];
            // dd($dataSale);


            $sale->update($dataSale);
        } 

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        

    }

    public function receiveDetails($id)
    {
       $receivedetails = $this->model->where('saleID',$id)->get(); 
    //    dd($receivedetails);
        return $receivedetails;
    }

}
