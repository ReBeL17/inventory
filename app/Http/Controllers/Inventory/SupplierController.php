<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Supplier;
use App\Models\Inventory\Suppliers_type;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class SupplierController extends Controller
{
    public $model, $type;

        
        public function __construct(Supplier $model, Suppliers_type $type)
        {

            $this->middleware('auth:admin');
            $this->model = $model;
            $this->type = $type;
           
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('suppliers-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $data = $this->model->with('suppliers_type')->paginate(40);
        $type = $this->type->pluck('name', 'id');
        return view('userbackend.Inventory.Supplier.index' , compact('data','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('userbackend.Inventory.Supplier.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'name' => 'required',
            // 'type_id' => 'required',
            // 'address' => 'required',
            // 'contact' => 'required',
            
        ]);

        $data = [
                    'name' => $request->name,
                    'type_id' => $request->type_id,
                    'address' => $request->address,
                    'contact' => $request->contact,
                    'status' => $request->status,
                ];

                // dd($data);
        $latest=$this->model->create($data);
        Session::flash('flash_success', 'Supplier created successfully!.');
            Session::flash('flash_type', 'alert-success');
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(Gate::denies('suppliers-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $model = $this->model->find($id);
        $data = $this->model->paginate(40);
        $type = $this->type->pluck('name','id');
        return view('userbackend.Inventory.Supplier.index' , compact('data','model','type'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $this->validate($request,[
            'name' => 'required',
            // 'type_id' => 'required',
            // 'address' => 'required',
            // 'contact' => 'required',
        ]);

        
        $data = [
                'name' => $request->name,
                'type_id' => $request->type_id,
                'status' => $request->status,
                'contact' => $request->contact,
                'address' => $request->address,
                ];

                // dd($data);
        $this->model->find($id)->update($data);
        Session::flash('flash_success', 'Supplier updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        abort_if(Gate::denies('suppliers-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $this->model->find($id)->delete(); 
        Session::flash('flash_danger', 'Supplier has been deleted!.');
        Session::flash('flash_type', 'alert-danger');
        return redirect()->back();

    }

    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('suppliers-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
