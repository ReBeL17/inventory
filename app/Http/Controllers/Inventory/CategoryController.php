<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Category;
use Auth;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    public $model;

        
        public function __construct(Category $model)
        {
            $this->middleware('auth:admin');
            $this->model = $model;
           
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('category-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $id=Auth::user()->id;
        $data = $this->model->paginate(40);
        
        return view('userbackend.Inventory.Category.index' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('userbackend.Inventory.Category.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'title' => 'required'
        ]);

        $data = ['title' => $request->title,
                    'description' => $request->description,
                    ];
        $latest=$this->model->create($data);
        Session::flash('flash_success', 'Category created successfully!.');
            Session::flash('flash_type', 'alert-success');
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        abort_if(Gate::denies('category-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $model = $this->model->find($id);
        $data = $this->model->paginate(40);

        return view('userbackend.Inventory.Category.index',compact('model','data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $this->validate($request,[
            'title' => 'required'
        ]);

        
        $data = ['title' => $request->title,
        'description' => $request->description,];
        $this->model->find($id)->update($data);
        Session::flash('flash_success', 'Category updated successfully!.');
            Session::flash('flash_type', 'alert-success');
            return redirect()->route('admin.categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        abort_if(Gate::denies('category-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $this->model->find($id)->delete(); 
        Session::flash('flash_danger', 'Category has been deleted!.');
        Session::flash('flash_type', 'alert-danger');
        return redirect()->back();

    }

    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('category-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
