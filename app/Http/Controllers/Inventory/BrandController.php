<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Brand;
use Auth;

class BrandController extends Controller
{
    public $model;

        
    public function __construct(Brand $model)
    {

        $this->model = $model;
       
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    //
    // $id=Auth::user()->id;
    $data = $this->model->paginate(40);
    // dd($data->all());
    return view('userbackend.Inventory.Brand.index' , compact('data'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    //
    // return view('userbackend.Inventory.Brand.index');
}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    // dd($request->all());
    $this->validate($request,[
        'name' => 'required',
        // 'product_id' => 'required'
    ]);

    $data = [
                'name' => $request->name,
                // 'product_id' => $request->product_id,
            ];
    // dd($data);
    $latest=$this->model->create($data);
    // $latest->products()->sync($request->product_id);
        return redirect()->back();
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    // dd($id);
    $model = $this->model->find($id);
    $data = $this->model->paginate(40);
    // $products = $this->product->pluck('title','id');

    return view('userbackend.Inventory.Brand.index',compact('model','data'));

}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
    // dd($request->all());
    $this->validate($request,[
        'name' => 'required',
        // 'product_id' => 'required'
    ]);

    $data = [
                'name' => $request->name,
                // 'product_id' => $request->product_id,
            ];
    $this->model->find($id)->update($data);
        return redirect('user/brands');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function delete($id)
{
    // dd('d');
    $this->model->find($id)->delete(); 
    return redirect()->back();

}
}
