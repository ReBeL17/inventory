<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Payment;
use Auth;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class ReceiptController extends Controller {

    private $model;
        
        public function __construct(Payment $model)
        {
            $this->middleware('auth:admin');
            $this->model = $model;
        }

        public function allReceipts(){
                abort_if(Gate::denies('receipt-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
                // dd('hello');
                $user = Auth::user();
                $receipts = $this->model->paginate(40);
                return view('userbackend.Inventory.receipts.index', compact('receipts'));
            }

            public function generateReceipt(Payment $receipt)
            {
                abort_if(Gate::denies('receipt-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
            
                $receipt->load('purchases');
                // dd($receipt);
                return view('userbackend.Inventory.receipts.edit', compact('receipt'));
            }

            public function printReceipt(Payment $receipt)
            {
                abort_if(Gate::denies('receipt-print'), Response::HTTP_FORBIDDEN, '403 Forbidden');
            
                $receipt->load('purchases','suppliers');
                
                return view('userbackend.Inventory.receipts.receipt-print', compact('receipt'));
            }
    }