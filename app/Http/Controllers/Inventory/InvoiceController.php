<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Receive;
use Auth;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends Controller
{
    private $model;
        
    public function __construct(Receive $model)
    {
        $this->middleware('auth:admin');
        $this->model = $model;
    }

    public function allInvoices(){
            abort_if(Gate::denies('invoice-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
            // dd('hello');
            $user = Auth::user();
            $receipts = $this->model->paginate(40);
            return view('userbackend.Inventory.Invoice.index', compact('receipts'));
        }

        public function generateInvoice(Receive $receipt)
        {
            abort_if(Gate::denies('invoice-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
            // dd($receipt);
            $receipt->load('sale');
            return view('userbackend.Inventory.Invoice.edit', compact('receipt'));
        }

        public function printInvoice(Receive $receipt)
        {
            abort_if(Gate::denies('invoice-print'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
            $receipt->load('sale','customer');
            
            return view('userbackend.Inventory.Invoice.receipt-print', compact('receipt'));
        }
}
