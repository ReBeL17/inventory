<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Sale;
use App\Models\Inventory\Product;
use App\Models\Inventory\Customer;
use App\Models\Inventory\Category;
use App\Models\Inventory\Product_Update;
use App\Models\Inventory\Purchase;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class SaleController extends Controller
{
    private $model, $product, $category, $customer, $p_update, $purchase;

        
    public function __construct(Sale $model, Product $product, Category $category, Customer $customer, Product_Update $p_update, Purchase $purchase)
    {
        $this->middleware('auth:admin');
        $this->model = $model;
        $this->product = $product;
        $this->category = $category;
        $this->customer = $customer;    
        $this->p_update = $p_update;  
        $this->purchase = $purchase;   

    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    //
    abort_if(Gate::denies('sale-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $id=Auth::user()->id;
    $data = $this->model->with('customer','products')->where('admin_id',$id)->paginate(40);
    $customers = $this->customer->pluck('name','id');
    $products = $this->product->pluck('title', 'id');
    $categories = $this->category->pluck('title', 'id');
    
    //sale ID creation
    $saleID = Sale::select('saleID')
    ->orderBy('saleID', 'desc')
    ->get();
    if(sizeof($saleID)==0)
    $saleID=10005;
    else
    $saleID= $saleID[0]->saleID+1;
    //  dd($saleID);
    return view('userbackend.Inventory.Sale.index' , compact('data','customers','products','categories','saleID'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    //
    // return view('userbackend.Inventory.Sale.index');
}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    // dd($request->all());
    $this->validate($request,[
        'customer_id' => 'required',
        'category_id' => 'required',
        'product_id' => 'required',
        'product_details' => 'required',
        'mark' => 'required',
        'total_qty' => 'required',
        'per_price' => 'required',

    ]);

    $data = [
            'product_id' => $request->product_id,
            'customer_id' => $request->customer_id,
            'product_details' => $request->product_details,
            'mark' => $request->mark,
            'total_qty' => $request->total_qty,
            'per_price' => $request->per_price,
            'saleID' => $request->saleID,
            'total_price' => $request->total_qty * $request->per_price,
            'due' => $request->total_qty * $request->per_price,
            'admin_id' => Auth::user()->id,

            ];

            // $avail = $this->p_update->where('product_id',$request->product_id)->first();
            if($request->avail >=  $request->total_qty){

                $latest=$this->model->create($data);
                $this->productUpdate($request->product_id);
                Session::flash('flash_success', 'Sales made successfully!.');
                Session::flash('flash_type', 'alert-success');
                return redirect()->back();
                
            }


            // $avail = $this->p_update->where('product_id',$request->product_id)->first();
            // $avail->avail_qty =$avail->avail_qty - $request->total_qty;
            // $avail->update();
            Session::flash('flash_danger' , 'Sales volume exceeded available stock!.');
            Session::flash('flash_type' , 'alert-danger');
        return redirect()->back();
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    abort_if(Gate::denies('sale-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $model = $this->model->find($id);
    $avail = $this->p_update->where('product_id',$model->product_id)->first();
    $ct = $model->products->categories;
    // dd($ct);
    $id=Auth::user()->id;
    $data = $this->model->with('customer','products')->where('admin_id',$id)->paginate(40);
    $customers = $this->customer->pluck('name','id');
    $products = $this->product->pluck('title', 'id');
    $categories = $this->category->pluck('title', 'id');

    // $brands = $this->brand->pluck('name', 'id');
    // dd($model->products->categories);
    return view('userbackend.Inventory.Sale.index' , compact('model','data','avail','customers','products','categories','ct'));

}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
    // dd($request->all());
    $this->validate($request,[
        'customer_id' => 'required',
        'category_id' => 'required',
        'product_id' => 'required',
        'product_details' => 'required',
        'mark' => 'required',
        'total_qty' => 'required',
        'per_price' => 'required',

    ]);

    $data = [
            'saleID' => $request->saleID,
            'product_id' => $request->product_id,
            'customer_id' => $request->customer_id,
            'product_details' => $request->product_details,
            'mark' => $request->mark,
            'total_qty' => $request->total_qty,
            'per_price' => $request->per_price,
            'total_price' => $request->total_qty * $request->per_price,
            'due' => $request->total_qty * $request->per_price,
            'admin_id' => Auth::user()->id,

            ];
            
        $old = $this->model->findOrFail($id);
        // dd($old->total_qty);

        // $avail = $this->p_update->where('product_id',$request->product_id)->first();
        // $sub = $request->total_qty - $old->total_qty;
        // dd($sub);
        if($request->avail >= ($request->total_qty - $old->total_qty)){
            $this->model->find($id)->update($data);
            $this->productUpdate($request->product_id);
            Session::flash('flash_success', 'Sales updated successfully!.');
            Session::flash('flash_type', 'alert-success');
            return redirect()->route('admin.sales');
        }
            // $avail = $this->p_update->where('product_id',$request->product_id)->first();
            // $avail->avail_qty =$avail->avail_qty - $request->total_qty;
            // $avail->update();
            Session::flash('flash_danger' , 'Sales failed to update!.');
            Session::flash('flash_type' , 'alert-danger');
        return redirect()->route('admin.sales');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function delete($id)
{
    abort_if(Gate::denies('sale-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $a = $this->model->find($id);
    $a->delete();
    $this->productUpdate($a->product_id);
    Session::flash('flash_danger' , 'The Sales has been Deleted!.');
    Session::flash('flash_type' , 'alert-danger');
    return redirect()->back();

}

public function productUpdate($product_id)
    {
        $sale_total = $this->model->where('product_id',$product_id)->sum('total_qty');
        $purchase_total = $this->purchase->where('product_id',$product_id)->sum('total_qty');
            $avail = $purchase_total - $sale_total;
            // dd($avail);
        // $pu = $this->p_update->where('product_id',$product_id)->first();
        // $new_avail = $pu->avail_qty + $total_qty;
        // return $this->p_update->updateOrCreate(['product_id'=>$product_id],['avail_qty'=>$new_avail]);
        return $this->p_update->updateOrCreate(['product_id'=>$product_id],['avail_qty'=>$avail]);

    }

    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('sale-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
