<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Customer;
use Auth;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    private $model;

        
    public function __construct(Customer $model)
    {
        $this->middleware('auth:admin');
        $this->model = $model;
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    //
    abort_if(Gate::denies('customer-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $data = $this->model->paginate(40);
    
    return view('userbackend.Inventory.Customer.index' , compact('data'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    //
    // return view('userbackend.Inventory.Customer.index');
}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    // dd($request->all());
    $this->validate($request,[
        'name' => 'required',
        // 'mobile' => 'required|unique:customers',
        // 'address' => 'required'
    ]);

    $data = [
                'name' => $request->name,
                'mobile' => $request->mobile,
                'address' => $request->address,
            ];
    $latest=$this->model->create($data);
    Session::flash('flash_success', 'Customer created successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->back();
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    abort_if(Gate::denies('customer-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $model = $this->model->find($id);
    $data = $this->model->paginate(40);

    return view('userbackend.Inventory.Customer.index',compact('model','data'));

}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
    // dd($request->all());
    $this->validate($request,[
        'name' => 'required',
        // 'mobile' => 'required',
        // 'address' => 'required'
    ]);

    $data = [
                'name' => $request->name,
                'mobile' => $request->mobile,
                'address' => $request->address,
            ];
    $this->model->find($id)->update($data);
    Session::flash('flash_success', 'Customer updated successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.customers');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function delete($id)
{
    abort_if(Gate::denies('customer-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $this->model->find($id)->delete(); 
    Session::flash('flash_success', 'Customer has been deleted !.');
            Session::flash('flash_type', 'alert-success');
    return redirect()->back();

}

public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('customer-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
