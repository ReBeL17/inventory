<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Product;
use App\Models\Inventory\Category;
use App\Models\Inventory\Brand;
use App\Models\Inventory\Product_Update;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class ProductController extends Controller
{
    private $model,$category,$brand,$p_update;

        
    public function __construct(Product $model, Category $category, Brand $brand, Product_Update $p_update)
    {
        $this->middleware('auth:admin');
        $this->model = $model;
        $this->category = $category;
        $this->brand = $brand;
        $this->p_update = $p_update;

    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    abort_if(Gate::denies('product-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $id=Auth::user()->id;
    $data = $this->model->with('categories')->paginate(40);
    $categories = $this->category->pluck('title','id');
    
    return view('userbackend.Inventory.Product.index' , compact('data','categories'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    //
    // return view('userbackend.Inventory.Product.index');
}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    // dd($request->all());
    $this->validate($request,[
        'title' => 'required|unique:product',
        'category_id' => 'required'
    ]);

    $data = [
                'title' => $request->title,
                'category_id' => $request->category_id,
            ];
    // dd($data);
    $latest=$this->model->create($data);
    Session::flash('flash_success', 'Product created successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->back();
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    abort_if(Gate::denies('product-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $model = $this->model->find($id);
    $data = $this->model->paginate(40);
    $categories = $this->category->pluck('title','id');

    return view('userbackend.Inventory.Product.index',compact('model','data','categories'));

}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
    // dd($request->all());
    $this->validate($request,[
        'title' => 'required',
        'category_id' => 'required'
    ]);

    $data = [
                'title' => $request->title,
                'category_id' => $request->category_id,
            ];
    $this->model->find($id)->update($data);
    Session::flash('flash_success', 'Product updated successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.products');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function delete($id)
{
    abort_if(Gate::denies('product-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $this->model->find($id)->delete(); 
    Session::flash('flash_danger', 'Product has been deleted!.');
            Session::flash('flash_type', 'alert-danger');
    return redirect()->back();

}

public function getSpecProducts(Request $request){
    // dd($request);
    $prod = $this->model->where('category_id',$request->category_id)->get();
    // $this->getAvailQty()
    return $prod;
}

public function getAvailQty(Request $request){
    $prod = $this->p_update->where('product_id',$request->product_id)->first();
     if($prod == null)
     {
         return 0;
     }
     else{
        return $prod;

     }
}

public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('product-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
