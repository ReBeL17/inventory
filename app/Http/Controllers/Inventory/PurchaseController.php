<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory\Purchase;
use App\Models\Inventory\Supplier;
use App\Models\Inventory\Product;
use App\Models\Inventory\Brand;
use App\Models\Inventory\Category;
use App\Models\Inventory\Sale;
use App\Models\Inventory\Product_Update;
use Session;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class PurchaseController extends Controller
{
    private $model, $supplier, $product, $brand, $category, $p_update, $sale;

        
    public function __construct(Purchase $model, Supplier $supplier, Product $product, Brand $brand, Category $category, Product_Update $p_update, Sale $sale)
    {
        $this->middleware('auth:admin');
        $this->model = $model;
        $this->supplier = $supplier;
        $this->product = $product;
        $this->brand = $brand;
        $this->category = $category;
        $this->p_update = $p_update;
        $this->sale = $sale;
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    abort_if(Gate::denies('purchase-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $id=Auth::user()->id;

    $data = $this->model->with('suppliers','products','brands')->where('admin_id',$id)->paginate(40);
    $suppliers = $this->supplier->where('status',1)->pluck('name','id');
    $products = $this->product->pluck('title', 'id');
    $categories = $this->category->pluck('title', 'id');
    // $brands = $this->brand->pluck('name', 'id');
    
    ///Purchase ID creation
        $purchaseID = Purchase::select('purchaseID')
        ->orderBy('purchaseID', 'desc')
        ->get();
        if(sizeof($purchaseID)==0)
        $purchaseID=55005;
        else
        $purchaseID= $purchaseID[0]->purchaseID+1;
        //  dd($purchaseID);

    return view('userbackend.Inventory.Purchase.index' , compact('data','suppliers','categories', 'products','purchaseID'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    //
    // return view('userbackend.Inventory.Purchase.index');
}

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    // dd($request->all());
    $this->validate($request,[
        'purchaseID' => 'required',
        'supplier_id' => 'required',
        'product_id' => 'required',
        'category_id' => 'required',
        // 'brand_id' => 'required',
        'product_details' => 'required',
        'mark' => 'required',
        'total_qty' => 'required',
        'per_price' => 'required',
    ]);

    $data = [
                'purchaseID' => $request->purchaseID,
                'supplier_id' => $request->supplier_id,
                'product_id' => $request->product_id,
                // 'brand_id' => $request->brand_id,
                'product_details' => $request->product_details,
                'mark' => $request->mark,
                'total_qty' => $request->total_qty,
                'per_price' => $request->per_price,
                'total_price' => $request->total_qty * $request->per_price,
                // 'avail_qty' => $request->total_qty,
                'due' => $request->total_qty * $request->per_price,
                'admin_id' => Auth::user()->id,
            ];
            // dd($data);
            $latest=$this->model->create($data);
            // dd($latest);

            $this->productUpdate($request->product_id);

            // $avail = $this->model->where('product_id',$request->product_id)->sum('avail_qty');
            // // dd($avail);
            // $this->p_update->updateOrCreate(['product_id'=>$request->product_id],['avail_qty'=>$avail]);
    // $data2 = [
    //             'purchaseID' => $latest->id,
    //             'total_category' => $latest->total_price,
    //             'due' => $latest->total_price,
    //         ];
            // dd($data2);  
            // $this->invoice->create($data2);
            Session::flash('flash_success', 'Purchases created successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->back();
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    abort_if(Gate::denies('purchase-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $model = $this->model->find($id);
    $ct = $model->products->categories;
    // dd($ct);
    $id=Auth::user()->id;
    $data = $this->model->with('suppliers','products')->where('admin_id',$id)->paginate(40);
    $suppliers = $this->supplier->pluck('name','id');
    $products = $this->product->pluck('title', 'id');
    $categories = $this->category->pluck('title', 'id');

    // $brands = $this->brand->pluck('name', 'id');
    // dd($model->products->categories);
    return view('userbackend.Inventory.Purchase.index' , compact('model','data','suppliers','products','categories','ct'));
   
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
    // dd($request->all());
    $this->validate($request,[
        'purchaseID' => 'required',
        'supplier_id' => 'required',
        'product_id' => 'required',
        'category_id' => 'required',
        // 'brand_id' => 'required',
        'product_details' => 'required',
        'mark' => 'required',
        'total_qty' => 'required',
        'per_price' => 'required',
    ]);

    $data = [
                'purchaseID' => $request->purchaseID,
                'supplier_id' => $request->supplier_id,
                'product_id' => $request->product_id,
                // 'brand_id' => $request->brand_id,
                'product_details' => $request->product_details,
                'mark' => $request->mark,
                'total_qty' => $request->total_qty,
                'per_price' => $request->per_price,
                'total_price' => $request->total_qty * $request->per_price,
                // 'avail_qty' => $request->total_qty,
                'due' => $request->total_qty * $request->per_price,
                'admin_id' => Auth::user()->id,
            ];
     $this->model->find($id)->update($data);

     $this->productUpdate($request->product_id);
    
    //  $avail = $this->model->where('product_id',$request->product_id)->sum('avail_qty');
    //         // dd($avail);
    //         $this->p_update->updateOrCreate(['product_id'=>$request->product_id],['avail_qty'=>$avail]);

    //  $data2 = [
    //     'total_category' => $data['total_price'],
    //     'due' => $data['total_price'],
    // ];
    // dd($data2); 
    // $this->invoice->where('purchaseID', $id)->update($data2);
    Session::flash('flash_success', 'Purchases updated successfully!.');
            Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.purchases');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function delete($id)
{
    abort_if(Gate::denies('purchase-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    $a = $this->model->find($id);
    $a->delete();
    $this->productUpdate($a->product_id);
    Session::flash('flash_danger', 'Purchases has been deleted!.');
            Session::flash('flash_type', 'alert-danger');
    return redirect()->back();

}

public function productUpdate($product_id)
    {
        $purchase_total = $this->model->where('product_id',$product_id)->sum('total_qty');
        $sale_total = $this->sale->where('product_id',$product_id)->sum('total_qty');
            $avail = $purchase_total - $sale_total;
        // $pu = $this->p_update->where('product_id',$product_id)->first();
        // $new_avail = $pu->avail_qty + $total_qty;
        // return $this->p_update->updateOrCreate(['product_id'=>$product_id],['avail_qty'=>$new_avail]);
        return $this->p_update->updateOrCreate(['product_id'=>$product_id],['avail_qty'=>$avail]);

    }

    public function massDestroy(Request $request)
    {
        abort_if(Gate::denies('purchase-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->input('ids')) {
            $entries = $this->model->whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
