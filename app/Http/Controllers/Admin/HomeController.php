<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\carbon;
use App\Setting;
use App\Models\Inventory\Product;
use App\Models\Inventory\Purchase;
use App\Models\Inventory\Sale;
use App\Models\Inventory\Product_Update;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

            $product_total = Purchase::selectRaw("sum(total_qty) as total,product_id")->with('products')
                            ->orderBy("product_id")
                            ->groupBy("product_id")
                            ->get();
                            // dd($product_total);
                            
            $product_supp = Purchase::selectRaw("sum(total_qty) as total,supplier_id")->with('products','suppliers')
            ->where("product_id",1)
            ->groupBy("supplier_id")
            ->get();
            // dd($product_supp);

            $product_avail = Product_Update::with('products')->select('product_id','avail_qty')->get();
            // dd($product_avail);

            // $data = [];
            // $data2 = [];
            $d = 0;
            $dt = Carbon::now();
          // dd($dt);
            $purchase_report = Purchase::selectRaw('sum(total_price) as purchase_total, MONTH(created_at) month')
            ->whereYear('created_at',$dt->year)
            ->groupBy('month')
            ->get();
            // dd($purchase_report->all());
              
                  for($i=1;$i<=12;$i++)
                  {
                    foreach($purchase_report as $k => $purchase)
                    {
                      // dd($purchase);
                     if($i==$purchase->month)
                      {
                        $d=$purchase->purchase_total;
                        break;
                      }  
                      $d=0;
                    }
                    $data[$i]=$d; 
                  }
            $sales_report = Sale::selectRaw('sum(total_price) as sales_total, MONTH(created_at) month')
            ->whereYear('created_at',$dt->year)
            ->groupBy('month')
            ->get();
            // dd($sales_report);
            // dd(!empty($sales_report));
              
                for($i=1;$i<=12;$i++)
                {
                foreach($sales_report as $sales)
                {
                if($i==$sales->month)
                    {
                      $d=$sales->sales_total;
                      break;
                    }  
                    $d=0;
                  }
                  $data2[$i]=$d; 
                }
            return view('admin.backend.index',compact('products','product_total','product_supp','product_avail','dt','data','data2'));
    }

    // public function drilldown() {
    //   dd('$id');
    //   $product_supp = Purchase::selectRaw("sum(total_qty) as total,supplier_id")->with('products','suppliers')
    //         ->where("product_id",$id)
    //         ->groupBy("supplier_id")
    //         ->get();
    // }

    public function getQuery(Request $request){
        if($request->type == 1){
          $query_result = Purchase::selectRaw('sum(total_qty) as sum, product_id')
          ->with('products')
          ->whereIn('product_id', $request->product)
          ->whereBetween('created_at', [$request->from, $request->to])
          ->groupBy('product_id')
          ->get();
          
          return $query_result;
        }
        elseif($request->type == 2){
          $query_result = Sale::selectRaw('sum(total_qty) as sum, product_id')
          ->with('products')
          ->whereIn('product_id', $request->product)
          ->whereBetween('created_at', [$request->from, $request->to])
          ->groupBy('product_id')
          ->get();
          
          return $query_result;
        }
      }
}
