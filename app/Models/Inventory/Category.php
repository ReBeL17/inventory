<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ['title','description'];

    // public function purchases(){
    //     return $this->hasMany(Purchase::class);
    // }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
