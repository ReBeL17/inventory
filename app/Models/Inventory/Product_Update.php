<?php

namespace App\models\inventory;

use Illuminate\Database\Eloquent\Model;

class Product_Update extends Model
{
    //
    protected $table = 'product_update';
    protected $fillable = ['product_id', 'avail_qty'];


    public function products(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
