<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    
    protected $fillable = ['title','category_id'];

    public function categories()
    {
        # code...
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function brands(){
        return $this->belongsToMany(Brand::class);
    }

    public function purchases(){
        return $this->hasMany(Purchase::class);
    }

    public function sales(){
        return $this->hasMany(Sale::class);
    }

    // public function product_updates(){
    //     return $this->belongsTo(Product_Update::class);
    // }
}
