<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $fillable = ['name', 'type_id', 'status','address','contact'];

    public function purchases(){
        return $this->hasMany(Purchase::class);
    }

    public function suppliers_type(){
        return $this->belongsTo(Suppliers_type::class, 'type_id');
    }
}
