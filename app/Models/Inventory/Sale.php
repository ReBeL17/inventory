<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //
    protected $guarded = [''];

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function products(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function receives(){
        return $this->hasMany(Receive::class);
    }
}
