<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = ['name','address','mobile'];

    public function sales(){
        return $this->hasMany(Purchase::class);
    }
}
