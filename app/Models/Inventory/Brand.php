<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $fillable = ['name','product_id'];
    
    public function products(){
        return $this->belongsToMany(Product::class)->withTimestamps();
    }

    public function purchases(){
        return $this->hasMany(Purchase::class);
    }
}
