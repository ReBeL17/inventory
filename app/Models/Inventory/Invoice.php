<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $guarded = [''];

    public function purchases(){
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }
}
