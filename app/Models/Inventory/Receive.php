<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Receive extends Model
{
    //

    protected $guarded = [''];
    
    public function sale(){
        return $this->belongsTo(Sale::class, 'saleID');
    }
    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }
    
}
