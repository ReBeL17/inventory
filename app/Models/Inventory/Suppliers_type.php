<?php

namespace App\models\inventory;

use Illuminate\Database\Eloquent\Model;

class Suppliers_type extends Model
{
    //
    protected $table = 'suppliers_type';
    protected $fillable = ['name'];

    public function suppliers(){
        return $this->hasMany(Suppliers::class);
    }
}
