<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //

    protected $guarded = [''];
    
    public function purchases(){
        return $this->belongsTo(Purchase::class, 'purchaseID');
    }
    public function suppliers(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
