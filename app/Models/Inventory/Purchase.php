<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    protected $guarded = [''];

    public function products(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function brands(){
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function suppliers(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function invoices(){
        return $this->hasOne(Invoice::class);
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }
}
