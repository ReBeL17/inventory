<!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('/backend/dist/img/AdminLTELogo.png')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
      <a href="{{ route('admin.dashboard')}}" class="d-block">{{Auth::user()->name}}</a>
      </div>
    </div>

    
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
      
        <li class="nav-item">
          <a href="{{ route('admin.dashboard')}}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p> <span> {{ trans('global.dashboard') }} </span></p>
          </a>
        </li>
        
            @can('user-management-access')
            <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/groups*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users">

                    </i>
                    <p>
                        <span>{{ trans('cruds.userManagement.title') }}</span>
                        <i class="right fa fa-fw fa-angle-left" style="right:.5rem;"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="margin-left: 1rem;">
                    @can('permission-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-unlock-alt">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.permission.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('role-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-briefcase">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.role.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('group-access')
                      <li class="nav-item">
                          <a href="{{ route("admin.groups.index") }}" class="nav-link {{ request()->is('admin/groups') || request()->is('admin/groups/*') ? 'active' : '' }}">
                            <i class="fa fa-users"></i>
                              <p>
                                  <span>{{ trans('cruds.group.title') }}</span>
                              </p>
                          </a>
                      </li>
                    @endcan
                    @can('user-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-user">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.user.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('supplier-management-access')
        <li class="nav-item has-treeview {{ request()->is('admin/suppliers_type*') ? 'menu-open' : '' }} {{ request()->is('admin/suppliers*') ? 'menu-open' : '' }} ">
          <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fa fa-ship">

              </i>
              <p>
                  <span>{{ trans('cruds.supplierManagement.title') }}</span>
                  <i class="right fa fa-fw fa-angle-left" style="right:.5rem;"></i>
              </p>
          </a>
          <ul class="nav nav-treeview" style="margin-left: 1rem;">
            @can('suppliertype-access')
                <li class="nav-item">
                  <a href="{{ route('admin.suppliers_type') }}" class="nav-link {{ request()->is('admin/suppliers_type') || request()->is('admin/suppliers_type/*') ? 'active' : '' }}">
                      <i class="fa fa-plus"></i>
                      <p> <span>{{ trans('menus.backend.sidebar.supplier_type') }}</span></p>
                  </a>
              </li>
            @endcan
            @can('suppliers-access')
              <li class="nav-item">
                  <a href="{{ route('admin.suppliers') }}" class="nav-link {{ request()->is('admin/suppliers') || request()->is('admin/suppliers/*') ? 'active' : '' }}">
                      <i class="fa fa-ship"></i>
                      <p> <span>{{ trans('menus.backend.sidebar.supplier') }}</span></p>
                  </a>
              </li>
            @endcan
          </ul>
        </li>
        @endcan

        @can('product-management-access')
        <li class="nav-item has-treeview {{ request()->is('admin/categories*') ? 'menu-open' : '' }} {{ request()->is('admin/products*') ? 'menu-open' : '' }} ">
          <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fa fa-plus-circle">

              </i>
              <p>
                  <span>{{ trans('cruds.productManagement.title') }}</span>
                  <i class="right fa fa-fw fa-angle-left" style="right:.5rem;"></i>
              </p>
          </a>
          <ul class="nav nav-treeview" style="margin-left: 1rem;">
            @can('category-access')
              <li class="nav-item">
                <a href="{{ route('admin.categories') }}" class="nav-link {{ request()->is('admin/categories') || request()->is('admin/categories/*') ? 'active' : '' }}">
                    <i class="fa fa-sitemap"></i>
                    <p> <span>{{ trans('menus.backend.sidebar.category') }}</span></p>
                </a>
              </li>
            @endcan
            @can('product-access')
              <li class="nav-item">
                  <a href="{{ route('admin.products') }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                          <i class="fa fa-plus-circle"></i>
                          <p> <span>{{ trans('menus.backend.sidebar.product') }}</span></p>
                  </a>
              </li>
            @endcan
          </ul>
        </li>
        @endcan

        @can('purchase-management-access')
        <li class="nav-item has-treeview {{ request()->is('admin/purchases*') ? 'menu-open' : '' }} {{ request()->is('admin/payments*') ? 'menu-open' : '' }} {{ request()->is('admin/receipts*') ? 'menu-open' : '' }} ">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="fa fa-cart-plus">

                </i>
                <p>
                    <span>{{ trans('cruds.purchaseManagement.title') }}</span>
                    <i class="right fa fa-fw fa-angle-left" style="right:.5rem;"></i>
                </p>
            </a>
            <ul class="nav nav-treeview" style="margin-left: 1rem;">
            @can('purchase-access')
              <li class="nav-item">
                  <a href="{{ route('admin.purchases') }}" class="nav-link {{ request()->is('admin/purchases') || request()->is('admin/purchases/*') ? 'active' : '' }}">
                      <i class="fa fa-cart-plus"></i>
                      <p> <span>{{ trans('menus.backend.sidebar.purchase') }}</span></p>
                  </a>
              </li>
            @endcan
            @can('payment-access')
              <li class="nav-item">
                <a href="{{ route('admin.payments') }}" class="nav-link {{ request()->is('admin/payments') || request()->is('admin/payments/*') ? 'active' : '' }}">
                    <i class="fa fa-credit-card"></i>
                    <p> <span>{{ trans('menus.backend.sidebar.payment') }}</span></p>
                </a>
              </li>
            @endcan
            @can('receipt-access')
              <li class="nav-item">
                <a href="{{ route("admin.receipts.allReceipts") }}" class="nav-link {{ request()->is('admin/receipts') || request()->is('admin/receipts/*') ? 'active' : '' }}">
                  <i class="far fa-money-bill-alt"></i>
                    <p>
                        <span>{{ trans('menus.backend.sidebar.receipt') }}</span>
                    </p>
                </a>
            </li>
            @endcan
            </ul>
        </li>
        @endcan

            @can('customer-access')
              <li class="nav-item">
                  <a href="{{ route('admin.customers') }}" class="nav-link {{ request()->is('admin/customers') || request()->is('admin/customers/*') ? 'active' : '' }}">
                      <i class="fa fa-user"></i>
                      <p> <span>{{ trans('menus.backend.sidebar.customer') }}</span></p>
                  </a>
              </li>
            @endcan

        @can('sale-management-access')
        <li class="nav-item has-treeview {{ request()->is('admin/sales*') ? 'menu-open' : '' }} {{ request()->is('admin/invoices*') ? 'menu-open' : '' }} {{ request()->is('admin/receives*') ? 'menu-open' : '' }} ">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="fa fa-money">

                </i>
                <p>
                    <span>{{ trans('cruds.saleManagement.title') }}</span>
                    <i class="right fa fa-fw fa-angle-left" style="right:.5rem;"></i>
                </p>
            </a>
            <ul class="nav nav-treeview" style="margin-left: 1rem;">
            @can('sale-access')
              <li class="nav-item">
                <a href="{{ route('admin.sales') }}" class="nav-link {{ request()->is('admin/sales') || request()->is('admin/sales/*') ? 'active' : '' }}">
                    <i class="fa fa-money"></i>
                    <p> <span>{{ trans('menus.backend.sidebar.sale') }}</span></p>
                </a>
              </li>
            @endcan
            @can('receive-access')
            <li class="nav-item">
              <a href="{{ route('admin.receives') }}" class="nav-link {{ request()->is('admin/receives') || request()->is('admin/receives/*') ? 'active' : '' }}">
                  <i class="fa fa-credit-card"></i>
                  <p> <span>{{ trans('menus.backend.sidebar.receive') }}</span></p>
              </a>
            </li>
          @endcan
          @can('invoice-access')
            <li class="nav-item">
              <a href="{{ route('admin.invoices.allInvoices') }}" class="nav-link {{ request()->is('admin/invoices') || request()->is('admin/invoices/*') ? 'active' : '' }}">
                <i class="far fa-money-bill-alt"></i>
                  <p>
                      <span>{{ trans('menus.backend.sidebar.invoice') }}</span>
                  </p>
              </a>
          </li>
          @endcan
          </ul>
      </li>
      @endcan

            <li class="nav-item">
                <a href="{{ route('admin.settings.create') }}" class="nav-link {{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'active' : '' }}">
                    <i class="fa fa-user"></i>
                    <p> <span>{{ trans('menus.backend.sidebar.setting') }}</span></p>
                </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('admin.password.create') }}" class="nav-link {{ request()->is('admin/change-password') || request()->is('admin/change-password/*') ? 'active' : '' }}">
                  <i class="fa fa-key"></i>
                  <p> <span>{{ trans('global.change_password') }}</span></p>
              </a>
            </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>

  <div class="sidebar sidebar_footer" style="text-align:center;">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <a href="{{route('admin.logout')}}" class="nav-link">
          <i class="nav-icon fas fa-sign-out-alt"></i>
            <p> <span>{{trans('global.logout')}}</span></p>
        </a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar -->