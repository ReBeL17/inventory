<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title') | {{$setting->title}}</title>
  <!-- favicon -->
  <link rel="shortcut icon" href="{{asset('storage/uploads/favicon/'.$setting->favicon)}}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  {{-- csrf-token for ajax post request --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
  <!-- Tempusdominus Bbootstrap 4 -->
  {{-- <link rel="stylesheet" href="{{asset('/backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}"> --}}
  <!-- iCheck -->
  {{-- <link rel="stylesheet" href="{{asset('/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}"> --}}
  <!-- JQVMap -->
  {{-- <link rel="stylesheet" href="{{asset('/backend/plugins/jqvmap/jqvmap.min.css')}}"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/backend/dist/css/adminlte.min.css')}}">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- datepicker -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/datepicker/css/bootstrap-datepicker3.min.css')}}">

  <link rel="stylesheet" href="{{ asset('/backend/bower_components/select2/dist/css/select2.min.css')}}">

  <!-- dataTables -->
  {{-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" /> --}}
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />

  <!-- summernote -->
  {{-- <link rel="stylesheet" href="{{asset('/backend/plugins/summernote/summernote-bs4.css')}}"> --}}
  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
  <style>
    .box {
        position: relative;
        border-radius: 3px;
        background: #fff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .1)
    }

    .box.box-primary {
        border-top-color: #3c8dbc
    }

    .box.box-info {
        border-top-color: #d2d6de
    }

    .box.box-danger {
        border-top-color: #dd4b39
    }

    .box.box-warning {
        border-top-color: #f39c12
    }

    .box.box-success {
        border-top-color: #00a65a
    }

    .box.box-default {
        border-top-color: #d2d6de
    }

    .box.box-custom {
        border-top-color: #00CDAC
    }

    .box.collapsed-box .box-body,
    .box.collapsed-box .box-footer {
        display: none
    }

    .box .nav-stacked>li {
        border-bottom: 1px solid #f4f4f4;
        margin: 0
    }

    .box .nav-stacked>li:last-of-type {
        border-bottom: none
    }

    .box.height-control .box-body {
        max-height: 300px;
        overflow: auto
    }

    .box .border-right {
        border-right: 1px solid #f4f4f4
    }

    .box .border-left {
        border-left: 1px solid #f4f4f4
    }

    .box.box-solid {
        border-top: 0
    }

    .box.box-solid>.box-header .btn.btn-default {
        background: transparent
    }

    .box.box-solid>.box-header .btn:hover,
    .box.box-solid>.box-header a:hover {
        background: rgba(0, 0, 0, .1) !important
    }

    .box.box-solid.box-default {
        border: 1px solid #d2d6de
    }

    .box.box-solid.box-default>.box-header {
        color: #444;
        background: #d2d6de;
        background-color: #d2d6de
    }

    .box.box-solid.box-default>.box-header .btn,
    .box.box-solid.box-default>.box-header a {
        color: #444
    }

    .box.box-solid.box-primary {
        border: 1px solid #3c8dbc
    }

    .box.box-solid.box-primary>.box-header {
        color: #fff;
        background: #3c8dbc;
        background-color: #3c8dbc
    }

    .box.box-solid.box-primary>.box-header .btn,
    .box.box-solid.box-primary>.box-header a {
        color: #fff
    }

    .box.box-solid.box-info {
        border: 1px solid #00c0ef
    }

    .box.box-solid.box-info>.box-header {
        color: #fff;
        background: #00c0ef;
        background-color: #00c0ef
    }

    .box.box-solid.box-info>.box-header .btn,
    .box.box-solid.box-info>.box-header a {
        color: #fff
    }

    .box.box-solid.box-danger {
        border: 1px solid #dd4b39
    }

    .box.box-solid.box-danger>.box-header {
        color: #fff;
        background: #dd4b39;
        background-color: #dd4b39
    }

    .box.box-solid.box-danger>.box-header .btn,
    .box.box-solid.box-danger>.box-header a {
        color: #fff
    }

    .box.box-solid.box-warning {
        border: 1px solid #f39c12
    }

    .box.box-solid.box-warning>.box-header {
        color: #fff;
        background: #f39c12;
        background-color: #f39c12
    }

    .box.box-solid.box-warning>.box-header .btn,
    .box.box-solid.box-warning>.box-header a {
        color: #fff
    }

    .box.box-solid.box-success {
        border: 1px solid #00a65a
    }

    .box.box-solid.box-success>.box-header {
        color: #fff;
        background: #00a65a;
        background-color: #00a65a
    }

    .box.box-solid.box-success>.box-header .btn,
    .box.box-solid.box-success>.box-header a {
        color: #fff
    }

    .box.box-solid>.box-header>.box-tools .btn {
        border: 0;
        box-shadow: none
    }

    .box.box-solid[class*=bg]>.box-header {
        color: #fff
    }

    .box .box-group>.box {
        margin-bottom: 5px
    }

    .box .knob-label {
        text-align: center;
        color: #333;
        font-weight: 100;
        font-size: 12px;
        margin-bottom: .3em
    }

    .box>.loading-img,
    .box>.overlay,
    .overlay-wrapper>.loading-img,
    .overlay-wrapper>.overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%
    }

    .box .overlay,
    .overlay-wrapper .overlay {
        z-index: 7;
        background: hsla(0, 0%, 100%, .7);
        border-radius: 3px
    }

    .box .overlay>.fa,
    .overlay-wrapper .overlay>.fa {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -15px;
        margin-top: -15px;
        color: #000;
        font-size: 30px
    }

    .box .overlay.dark,
    .overlay-wrapper .overlay.dark {
        background: rgba(0, 0, 0, .5)
    }

    .box-body:after,
    .box-body:before,
    .box-footer:after,
    .box-footer:before,
    .box-header:after,
    .box-header:before {
        content: " ";
        display: table
    }

    .box-body:after,
    .box-footer:after,
    .box-header:after {
        clear: both
    }

    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative
    }

    .box-header.with-border {
        border-bottom: 1px solid #f4f4f4
    }

    .collapsed-box .box-header.with-border {
        border-bottom: none
    }

    .box-header .box-title,
    .box-header>.fa,
    .box-header>.glyphicon,
    .box-header>.ion {
        display: inline-block;
        font-size: 18px;
        margin: 0;
        line-height: 1
    }

    .box-header>.fa,
    .box-header>.glyphicon,
    .box-header>.ion {
        margin-right: 5px
    }

    .box-header>.box-tools {
        position: absolute;
        right: 10px;
        top: 5px
    }

    .box-header>.box-tools [data-toggle=tooltip] {
        position: relative
    }

    .box-header>.box-tools.pull-right .dropdown-menu {
        right: 0;
        left: auto
    }

    .btn-box-tool {
        padding: 5px;
        font-size: 12px;
        background: transparent;
        box-shadow: none !important;
        color: #97a0b3
    }

    .btn-box-tool:hover,
    .open .btn-box-tool {
        color: #606c84
    }

    .btn-box-tool:active {
        outline: none !important
    }

    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 10px
    }

    .no-header .box-body {
        border-top-right-radius: 3px;
        border-top-left-radius: 3px
    }

    .box-body>.table {
        margin-bottom: 0
    }

    .box-body .fc {
        margin-top: 5px
    }

    .box-body .full-width-chart {
        margin: -19px
    }

    .box-body.no-padding .full-width-chart {
        margin: -9px
    }

    .box-body .box-pane {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 3px
    }

    .box-body .box-pane-right {
        border-bottom-left-radius: 0
    }

    .box-body .box-pane-right,
    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px
    }

    .box-footer {
        border-bottom-left-radius: 3px;
        border-top: 1px solid #f4f4f4;
        padding: 10px;
        background-color: #fff
    }

    @media (min-width:768px) {
        .form-inline .form-group {
            display: inline-block;
            margin-bottom: 0;
            vertical-align: middle
        }

        .form-inline .form-control {
            display: inline-block;
            width: auto;
            vertical-align: middle
        }

        .form-inline .form-control-static {
            display: inline-block
        }

        .form-inline .input-group {
            display: inline-table;
            vertical-align: middle
        }

        .form-inline .input-group .form-control,
        .form-inline .input-group .input-group-addon,
        .form-inline .input-group .input-group-btn {
            width: auto
        }

        .form-inline .input-group>.form-control {
            width: 100%
        }

        .form-inline .control-label {
            margin-bottom: 0;
            vertical-align: middle
        }

        .form-inline .checkbox,
        .form-inline .radio {
            display: inline-block;
            margin-top: 0;
            margin-bottom: 0;
            vertical-align: middle
        }

        .form-inline .checkbox label,
        .form-inline .radio label {
            padding-left: 0
        }

        .form-inline .checkbox input[type=checkbox],
        .form-inline .radio input[type=radio] {
            position: relative;
            margin-left: 0
        }

        .form-inline .has-feedback .form-control-feedback {
            top: 0
        }
    }

    .form-horizontal .checkbox,
    .form-horizontal .checkbox-inline,
    .form-horizontal .radio,
    .form-horizontal .radio-inline {
        margin-top: 0;
        margin-bottom: 0;
        padding-top: 7px
    }

    .form-horizontal .checkbox,
    .form-horizontal .radio {
        min-height: 27px
    }

    .form-horizontal .form-group {
        margin-left: -15px;
        margin-right: -15px
    }

    .form-horizontal .form-group:after,
    .form-horizontal .form-group:before {
        content: " ";
        display: table
    }

    .form-horizontal .form-group:after {
        clear: both
    }

    @media (min-width:992px) {
        .form-horizontal .control-label {
            text-align: right;
            margin-bottom: 0;
            padding-top: 7px
        }
    }

    .form-horizontal .has-feedback .form-control-feedback {
        right: 15px
    }

    @media (min-width:768px) {
        .form-horizontal .form-group-lg .control-label {
            padding-top: 11px;
            font-size: 18px
        }
    }

    @media (min-width:768px) {
        .form-horizontal .form-group-sm .control-label {
            padding-top: 6px;
            font-size: 12px
        }
    }
    .pull-right {
        float: right !important
    }
</style>
  @yield('styles')
