<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invoice | MangoSoft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/backend/dist/css/adminlte.min.css')}}">

  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
</head>
<body>

  <div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h4>
          <i class="fas fa-globe"></i> Mango Soft Solution.
          <small class="float-right">Date: {{\Carbon\Carbon::now()->toDateString()}}</small>
        </h4>
      </div>
      <!-- /.col -->
    </div>
    <hr>
    <div class="row invoice-info" style="margin: 10px 0px 30px 0px;">
      <div class="col-sm-6 invoice-col">
      <strong>Invoice No. </strong>{{$receipt->sale->saleID}} <br>
      <strong>Payment Date:</strong> {{$receipt->created_at->toDateString()}}<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <h5>Customer Details :</h5>
    <div class="row invoice-info" style="border: 1px solid rgb(167, 167, 167);">
      <div class="col-sm-6 invoice-col">
        <strong>Name : </strong>{{$receipt->sale->customer->name}} <br>
        <strong>Address : </strong>{{$receipt->sale->customer->address}} <br>
      </div>
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">
        <strong>Contact : </strong>{{$receipt->sale->customer->contact}} <br>
        {{-- <strong>Batch : </strong>{{$receipt->student->batch->name}} <br> --}}
      </div>
      <!-- /.col -->
     
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row" style="margin: 40px 15px 15px 15px;">
      <div class="col-12 table-responsive">
        <table class="table table-striped" style="margin-top:10px;">
          <thead>
              <tr>
                  <th>Particulars</th>
                  <th>Unit</th>
                  <th>Price</th>
                  <th>Amount</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$receipt->sale->product_details}} </td>
              @if($receipt->paid === $receipt->sale->total_price)
                <td>{{$receipt->sale->total_qty}}</td>
                <td>{{$receipt->sale->per_price}}</td>
              @else
                <td></td>
                <td></td>
              @endif
              <td>{{$receipt->paid}}</td>
            </tr>
            {{-- @foreach($receipt->fine_payments as $fine) --}}
            <tr>
              {{-- <td>{{$fine->fine_title}}</td>
              <td>{{$fine->unit}}</td>
              <td>{{$fine->price}}</td>
              <td>{{$fine->amount}}</td> --}}
            </tr>
            {{-- @endforeach --}}
          </tbody>
          <tfoot>
              <tr>
                  <td colspan="3" class="text-right"><b>Total:</b></td>
                  <td>{{$receipt->paid}}</td>
              </tr>
          </tfoot>
      </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    {{-- <div class="row">
      <label>Narration :</label>
    </div> --}}

    <div class="row">
      <div class="col-sm-9">
        <label>Remarks : </label>  
      {{ $receipt->remarks}}
      </div>
    </div>

    <div class="row">
      <div class="col-sm-9">
        <label>Created by : </label>  
      {{ auth()->user()->name}}
      </div>
    </div>
    <!-- /.row -->
  <script type="text/javascript"> 
    window.addEventListener("load", window.print());
  </script>

</body>
</html>






