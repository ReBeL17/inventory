@extends('userbackend.inventory.layouts.user_app')

{{-- {{dd($data)}} --}}


@section('content')
{{-- @include('userbackend._partial.formheader') --}}

{{-- @include('userbackend.include.header') --}}
<hr>
<div class="container">
    <div class="banner-item-wrapper">
        <div class="row" style="background-color:#B2B2B2; margin-bottom:20px; padding:20px;">
            <form class="form-model" id="form">
            <div class="text-center">
                <i class="fa fa-bar-chart" style="font-size:24px">  <b>Generate your own chart</b></i>
            </div>
            <hr>
            <div class="col-md-2">
                    <label class="control-label col-sm-3" for="type">Type:</label>
                    <div class="col-sm-9">
                      <select class="form-control" id="type" style="width: 100%;" required>
                        <option value="">Select search type</option>
                        <option value="1">Purchase</option>
                          <option value="2">Sale</option>
                      </select>
                    </div>
            </div>
            
            <div class="col-md-3">
                    <label class="control-label col-sm-3" for="product">Product:</label>
                    <div class="col-sm-9">
                     <select class="form-control select2" multiple="multiple"
                          name="products[]" id="products" style="width: 100%;" required>
                         @foreach($products as $product)
                         <option value="{{$product->id}}">{{$product->title}}</option>                            
                         @endforeach
                     </select>
                   </div>
            </div>
            <div class="col-md-3">
                    <label class="control-label col-sm-3" for="from">From:</label>
                    <div class="col-sm-9">
                    <input type="date" id="from" class="form-control" required>
                   </div>
            </div>
            <div class="col-md-3">
                    <label class="control-label col-sm-3" for="to">To:</label>
                    <div class="col-sm-9">
                     <input type="date" id="to"  class="form-control" required>
                   </div>
            </div>
            <div class="col-md-1">
                    <button  id="display" type="button" class="btn btn-success btn-sm" onclick="getQuery();"><i class="fa fa-search"></i> Search</button>
            </div>          
        </form>
        </div>
        <div class="modal fade" id="barsearch">
                <!-- Bar chart -->
                <div class="modal-dialog">
                    <div class="modal-content">
                            <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-body">
                            <div style=""></div>
                            <div id="bar_query"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    <!-- /.box-body-->
                </div>
                    </div>
                </div>
                <!-- /.box -->
        </div>
        
        <div class="row">
            <div class="col-md-6">
                    <!-- Bar chart for available quantity-->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="box-body">
                                <div style=""></div>
                                <div id="line-graph"
                                    style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                            </div>
                        </div>
                        <!-- /.box-body-->
                    </div>
                    <!-- /.box -->
                </div>

            <div class="col-md-6">
                <!-- Bar chart for available quantity-->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-body">
                            <div style=""></div>
                            <div id="bar_avail"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-6">
                <!-- Bar chart -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-body">
                            <div style=""></div>
                            <div id="bar"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-6">
                <!-- Bar chart -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-body">
                            <div style=""></div>
                            <div id="pie"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </div>
</div>


        @endsection


        @section('after-scripts')
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>
        
        
        <script>
         
                $(document).ready(function(){
                        $('.select2').select2({
                            placeholder: "Select product to compare",
                            allowClear: true
                        });
        
                    });
        
        
            Highcharts.chart('line-graph', {
        
            title: {
                text: 'Purchases and Sales Curve in {{$dt->year}}'
            },
        
            xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
            yAxis: {
                title: {
                    text: 'Total Amount'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
        
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 0
                    
                }
            },
        
            series: [{
                name: 'Purchase',
                data: [
                        @foreach($data as $purchase)
                            {{$purchase}},
                            @endforeach
                        
                        ]
                },
        
                {
                name: 'Sales',
                data: [
                        @foreach($data2 as $sale)
                            {{$sale}},
                            @endforeach
                        ]
                },
            ],
        
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 800
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        
            });
        
        // Radialize the colors
        Highcharts.setOptions({
            colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            })
        });
        
        // Build the pie chart
        Highcharts.chart('pie', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Purchase Volume of Products'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Products',
                data: [
                    @foreach($product_total as $p) {
                        name: "{{$p->products->title}}",
                        y: {{$p->total}}
                    },
                    @endforeach
        
                ]
            }]
        });
        
            // Create the bar chart
            Highcharts.chart('bar', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Amount of Products Purchased'
                },
                subtitle: {
                    text: 'Click column to view details of product'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Quantity'
                    }
        
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
        
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} (qty.)</b> <br/>'
                },
        
                series: [
                    {
                        name: "Products",
                        colorByPoint: true,
                        data: [ 
                                @foreach($product_total as $p) 
                                {name: "{{$p->products->title}}", y: {{$p->total}}, drilldown: "{{$p->products->title}}" },
                                @endforeach
                             ]
                    }
                ],
                drilldown: {
                    series: [
                                @foreach($product_total as $p)
                                {name:"{{$p->products->title}}", id:"{{$p->products->title}}", 
                                data: 
                                
                                [
                                     @foreach($product_supp as $s)
                                     ["{{$s->suppliers->name}}", {{$s->total}}],
                                      @endforeach
                                    ] 
                                },
                                @endforeach
                      
                    ]
        
                }
            });
        
            // Create the bar chart
            Highcharts.chart('bar_avail', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Available Products in Stock'
                },            
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total Quantity'
                    }
        
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
        
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} (qty.)</b> <br/>'
                },
        
                series: [
                    {
                        name: "Products",
                        colorByPoint: true,
                        data: [ 
                                @foreach($product_avail as $product) 
                                {name: "{{$product->products->title}}", y: {{$product->avail_qty}} },
                                @endforeach     
                             ]
                    }
                ],
                
            });
        
        
        
            function getQuery(){        
                var type = $('#type').val();
                var product = $('#products').val();
                var from = $('#from').val();
                var to = $('#to').val();
        
                if (type=="" || product=="" || from=="" || to=="") {
                    alert("Fill all the details of your search");
                }
                else{
                var searchDetails = {
                    type: type,
                    product: product,
                    from: from,
                    to: to
                };
                $.ajax({
                    data: searchDetails,
                    url: 'getquery',
                    type: 'post',
        
                    success: function(data) {
                    // console.log(data);
                                $('#barsearch').modal('show');  
                                var resultData = [];
                                $.each(data, function (i, value) {
                                    resultData.push({
                                    name:value.products.title, y: parseInt(value.sum)
                                    });
                                });
                            Highcharts.chart('bar_query',{
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Your search result looks like this'
                            },            
                            accessibility: {
                                announceNewData: {
                                    enabled: true
                                }
                            },
                            xAxis: {
                                type: 'category'
                            },
                            yAxis: {
                                title: {
                                    text: 'Total Quantity'
                                }
        
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                series: {
                                    borderWidth: 0,
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.y}'
                                    }
                                }
                            },
        
                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} (qty.)</b> <br/>'
                            },
        
                            series: [
                                {
                                    name: "Products",
                                    colorByPoint: true,
                                    data: resultData
                                    
                                }
                            ],
                            
                        });
                    }
                });
                }
            }
        
            </script>
        @endsection