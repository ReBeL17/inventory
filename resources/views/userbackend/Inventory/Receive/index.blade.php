@extends('admin.backend.layouts.master')
@section('title','Collect Payments')

@section('styles')
   <style>
       .label-danger{
            color: red;
       }
       .label-success{
            color: green;
       }
       .label-warning{
            color: #f39c12;
       }
       .disabled {
            pointer-events: none;
        }
    </style> 
@endsection

@section('content')

<script>
    var customerID=0;
    var paidOrginal=0;
    function getProducts(id,r,price,IDsupp) {
        
        $.ajax({
            url: 'receives/receivedetails/' + id,
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                console.log(response);
                    document.getElementById('id').disabled = true;
                    document.getElementById('saleID').disabled = true;
                    document.getElementById('total_price').disabled = true;
                    document.getElementById('amountpaid').disabled = true;

                    document.getElementById('id').value = id;
                    document.getElementById('saleID').value = r;
                    document.getElementById('total_price').value = price;
                    document.getElementById('amountpaid').value = 0;
                    document.getElementById('amountpaidNew').value=0;
                    document.getElementById('remarks').value = "";
                    customerID= IDsupp;




                $.each(response, function (i, data) {
                    console.log(data);
                    paidOrginal = parseFloat(document.getElementById('amountpaid').value)+parseFloat(data.paid);
                    document.getElementById('amountpaid').value = paidOrginal;
                });
                    
            }
        });
    }

    function  calculatePayment() {
            var newpaid =document.getElementById('amountpaidNew').value;
            var bal =document.getElementById('total_price').value;

            var paid =0;
            if(newpaid=="");
            // else if(parseFloat(newpaid)<0){
            //     paid = parseFloat(paidOrginal) + parseFloat(newpaid);
            //     document.getElementById('amountpaid').value = paid;

            // }
            else {
                paid = parseFloat(paidOrginal) + parseFloat(newpaid);
                // console.log(paid);
                if (paid <= parseFloat(bal) )
                    document.getElementById('amountpaid').value = paid;
                else{
                    alert("Can't greater than total invoice Price");
                    document.getElementById('amountpaidNew').value = 0;
                    document.getElementById('amountpaid').value = paidOrginal;

                }
            }

        }

        function payToSupplyer(){
            var id =document.getElementById('id').value;
            var saleID =document.getElementById('saleID').value;
            var total_price = document.getElementById('total_price').value;
            var paid =paidOrginal;
            var newpaid =document.getElementById('amountpaidNew').value;
            var remarks =document.getElementById('remarks').value;

            var totalPaid=parseFloat(paidOrginal)+parseFloat(newpaid);
            //console.log(totalPaid);


            if(parseFloat(total_price)>=totalPaid) {
                if(newpaid!=0) {

                    var receiveDetails = {
                        customerID: customerID,
                        id: id,
                        saleID: saleID,
                        total: total_price,
                        oldpaid: paid,
                        newpaid: newpaid,
                        remarks: remarks
                    };
                    $.ajax({
                        data: {data: receiveDetails},
                        url: 'receives/store',
                        type: 'POST',

                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            // console.log(response);
                            location.reload();  
                            alert('Payment Successful');

                        }
                    });
                }
                else{
                    alert("Zero balance can't be paid.");

                }
            }
            else{
                alert("Paid amount can't greater than total amount.");

            }


            }


    </script>



<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                Collect Payments
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-striped table-hover datatable datatable-receive">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>SN</th>
                            <th>Status</th>
                            <th>SaleID</th>
                            <th>Customer Name</th>
                            <th>Product</th>
                            <th>Product Details</th>
                            <th>Total(qty.)</th>
                            <th>Price(per unit)</th>
                            <th>Due(amount)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $field)
                        <tr data-entry-id="{{ $field->id }}">
                            <td></td>
                            <td>{{$loop->index + 1}}</td>
                            <td>
                            @if($field->status == 0)
                                <label class="label label-danger">unpaid</label>
                            @elseif($field->status == 2)
                                <label class="label label-success">paid</label>
                            @else
                                <label class="label label-warning">partially paid</label>
                            @endif
                            </td>
                            <td>{!! $field->saleID !!}</td>
                            <td>{!! $field->customer->name !!}</td>
                            <td>{!! $field->products->title !!}</td>
                            <td>{!! $field->product_details !!}</td>                           
                            <td>{!! $field->total_qty !!}</td>                           
                            <td>{!! $field->per_price !!}</td>                           
                            <td>{!! $field->due !!}</td>                           

                            @can('receive-create')
                            <td>
                                    @if($field->status==0 || $field->status==1)
                                        <a class="btn btn-danger btn-sm" data-toggle="modal" 
                                            data-target="#addPayment" 
                                            onclick="getProducts({{$field->id}},{{$field->saleID}},{{$field->total_price}},{{$field->customer->id}})"
                                            style="cursor: pointer;"
                                            >
                                                <i class="fa fa-plus-circle"></i> Collect
                                    </a>
                                    @else
                                        <a class="btn btn-danger btn-sm disabled"  >
                                            <i class="fa fa-plus-circle"></i> Collect
                                    </a>
                                    @endif
                            </td>
                            @endcan
                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>


   <div class="modal fade" id="addPayment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Collect Payment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table  class="table">

                        <tbody>
                            <input type="hidden" name="id" id="id"/>
                        <tr>
                            <td></td>
                            <td>SaleID</td>


                            <td><input type="number" name="saleID" id="saleID"/></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td>Total</td>


                            <td><input type="number" name="total_price" id="total_price"/></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td>Paid</td>


                            <td><input type="number" name="paid" id="amountpaid"/></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td><label class="control-label">New Payment:</label></td>


                            <td><input type="number" onkeyup="calculatePayment()" name="newpaid" id="amountpaidNew" oninput="this.value = Math.abs(this.value)"/></td>

                        </tr>
                       
                        <tr>
                            <td></td>
                            <td><label class="control-label">Remarks:</label></td>


                            <td><textarea name="remarks" id="remarks"> </textarea></td>

                        </tr>



                        </tbody>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="payToSupplyer()">Collect</button>
                </div>
            </div>
           
        </div>
 
    </div>

@stop



@section('after-scripts')
    <script mobile="text/javascript">
        CKEDITOR.replace('pralu');
    </script>
@endsection

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
// @can('receipt-delete')
//   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
//   let deleteButton = {
//     text: deleteButtonTrans,
//     url: "{{ route('admin.purchases.massDestroy') }}",
//     className: 'btn-danger',
//     action: function (e, dt, node, config) {
//       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
//           return $(entry).data('entry-id')
//       });

//       if (ids.length === 0) {
//         alert('{{ trans('global.datatables.zero_selected') }}')

//         return
//       }

//       if (confirm('{{ trans('global.areYouSure') }}')) {
//         $.ajax({
//           headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
//           method: 'POST',
//           url: config.url,
//           data: { ids: ids, _method: 'DELETE' }})
//           .done(function () { location.reload() })
//       }
//     }
//   }
//   dtButtons.push(deleteButton)
// @endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-receive:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection