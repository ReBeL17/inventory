@extends('admin.backend.layouts.master')
@section('title','Sales')

@section('content')
@can('sale-create')
<div class="col-md-12">
    @if(isset ($model))
    {{-- {{dd($model)}} --}}
    {{ Form::model($model, ['route' => ['admin.sales.update', $model], 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
    {{ Form::open(['route' => 'admin.sales.store', 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ (isset ($model)) ? 'Update Sale' : 'Add Sale' }}
            </h3>

        </div>
        <div class="box-body">

            <div class="form-group row">
                {{ Form::label('saleID','SaleID', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('saleID')) has-error @endif ">
                @if(isset ($model))
                {{ Form::number('saleID', null, ['class' => 'form-control', 'required', 'readonly'])}}
                @else
                {{ Form::number('saleID', $saleID, ['class' => 'form-control', 'required', 'readonly'])}}
                @endif
                @if ($errors->has('saleID')) <p class="help-block">{{ $errors->first('saleID') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                    {{ Form::label('customer_id','Customer', ['class' => 'col-lg-2 control-label required']) }}
    
                    <div class="col-lg-10  @if($errors->has('customer_id')) has-error @endif ">
                        {{ Form::select('customer_id', $customers, null, ['class' => 'form-control', 'placeholder' =>'Pick a customer...', 'required']) }}
                        @if ($errors->has('customer_id')) <p class="help-block">{{ $errors->first('customer_id') }}</p> @endif
    
                    </div>
                    <!--col-lg-10-->
                </div>
                <!--form control-->
            
            <div class="form-group row">
                    {{ Form::label('category_id','Category', ['class' => 'col-lg-2 control-label required']) }}
    
                <div class="col-lg-10  @if($errors->has('category_id')) has-error @endif ">
                    @if(!isset ($model))
                    {{ Form::select('category_id', $categories, null, ['class' => 'form-control', 'placeholder' =>'Pick a category...', 'id' => 'sel_category', 'required']) }}
                    @else
                    <select name='category_id' class='form-control' id='sel_category'>
                            <option value="{{$ct->id}}">{{$ct->title}}</option>
                            @foreach ($categories as $category => $c)
                            <option value="{{$category}}">{{$c}}</option>
                            @endforeach
                    </select>
                    @endif
                    @if ($errors->has('category_id')) <p class="help-block">{{ $errors->first('category_id') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                    {{ Form::label('product_id','Product', ['class' => 'col-lg-2 control-label required']) }}
    
                <div class="col-lg-10  @if($errors->has('product_id')) has-error @endif ">
                        @if(!isset ($model))
                            {{ Form::select('product_id',[] , null, ['class' => 'form-control', 'placeholder' =>'Pick a product...', 'id' => 'sel_product', 'required']) }}
                        @else
                        <select name='product_id' class='form-control' id='sel_product'>
                            @foreach($products as $product => $value)
                                
                                @if($product == $model->product_id)
                                <option value="{{$model->product_id}}">{{$value}}</option>
                                @endif
                            @endforeach
                        </select>
                            {{-- {{ Form::select('product_id',$products , null, ['class' => 'form-control', 'placeholder' =>'Pick a product...', 'id' => 'sel_product', 'required']) }} --}}
                        @endif
                    @if ($errors->has('product_id')) <p class="help-block">{{ $errors->first('product_id') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('product_details','Product Details', ['class' => 'col-lg-2 control-label required']) }}

                <div class="col-lg-10  @if($errors->has('product_details')) has-error @endif ">
                    {{ Form::textarea('product_details', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the product details', 'required']) }}
                    @if ($errors->has('product_details')) <p class="help-block">{{ $errors->first('product_details') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('mark','Mark', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('mark')) has-error @endif ">
                        {{ Form::radio('mark', 0, ['class' => 'form-control'])}} Fresh
                        {{ Form::radio('mark', 1, ['class' => 'form-control']) }} Used
                    @if ($errors->has('mark')) <p class="help-block">{{ $errors->first('mark') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            
            <div class="form-group row inline">
                {{ Form::label('total_qty','Total(qty)', ['class' => 'col-lg-2 control-label required']) }}
                
                <div class="col-lg-5  @if($errors->has('total_qty')) has-error @endif ">
                        {{ Form::number('total_qty', NULL, ['class' => 'form-control', 'placeholder' =>'Enter total quantity of product', 'required',]) }}
                    @if ($errors->has('total_qty')) <p class="help-block">{{ $errors->first('total_qty') }}</p> @endif

                </div>
                <!--col-lg-10-->
                {{ Form::label('avail','Available Qty.', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-1">
                        @if(!isset ($model))
                        {{ Form::text('avail', null, ['class' => 'form-control', 'placeholder' =>'0', 'id' => 'avail', 'readonly']) }}
                        @else
                        {{ Form::text('avail', $avail->avail_qty, ['class' => 'form-control', 'placeholder' =>'0', 'id' => 'avail', 'readonly']) }}
                        @endif
                    </div>
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('per_price','Price per unit', ['class' => 'col-lg-2 control-label required']) }}
                
                <div class="col-lg-10  @if($errors->has('per_price')) has-error @endif ">
                        {{ Form::number('per_price', NULL, ['class' => 'form-control', 'placeholder' =>'Enter per unit price of product ', 'step' => 'any', 'required']) }}
                        @if ($errors->has('per_price')) <p class="help-block">{{ $errors->first('per_price') }}</p> @endif
                        
                    </div>
                    <!--col-lg-10-->
                </div>
                <!--form control-->
                
           
            </div>
            <div class="box box-info">
            <div class="box-body">
                
                <div class="pull-right">
                    {{ link_to_route('admin.sales', trans('Cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                
                    @if(isset($model))
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-info btn-xs']) }}
                    @else
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                    @endif
                </div>
                <!--pull-right-->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@endcan

<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                List of Sales
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-striped table-hover datatable datatable-sale">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>SN</th>
                            <th>SaleID</th>
                            <th>Customer Name</th>
                            <th>Product</th>
                            <th>Product Details</th>
                            <th>Mark</th>
                            <th>Total(qty.)</th>
                            <th>Price(per unit)</th>
                            <th>Total(amount)</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index = 0;?>
                        @foreach($data as $field)
                        <tr data-entry-id="{{ $field->id }}">
                            <td></td>
                            <td>{!! ++$index !!}</td>
                            <td>{!! $field->saleID !!}</td>
                            <td>{!! $field->customer->name !!}</td>
                            <td>{!! $field->products->title !!}</td>
                            <td>{!! $field->product_details !!}</td>                           
                            <td>{{($field->mark == 0) ? 'Fresh' : 'Used'}}</td>                           
                            <td>{!! $field->total_qty !!}</td>                           
                            <td>{!! $field->per_price !!}</td>                           
                            <td>{!! $field->total_price !!}</td>                           
                            <td>{!! ($field->status == 0)?'<label style="color:red">unpaid</label>':'<label style="color:green">paid</label>' !!}</td>                           

                            <td>
                                @can('sale-edit')
                                {!! link_to_route('admin.sales.edit', '', array($field->id),
                                array('class' => 'fa fa-pencil-square-o fa-fw')) !!}
                                @endcan
                                @can('sale-delete')
                                {!! link_to_route('admin.sales.delete', '', array($field->id),
                                array('class' => 'fa fa-trash','onclick'=>"return confirm('Are you sure?')")) !!}
                                @endcan
                            </td>

                            {!! Form::close() !!}


                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>




@stop

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('sale-delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.sales.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-sale:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection

@section('after-scripts')
    <script mobile="text/javascript">
        CKEDITOR.replace('pralu');
    </script>


{{-- get products of specific category --}}
    <script>
        $(document).ready(function(){
    $("#sel_category").change(function(){
        var categoryid = $(this).val();
        // console.log(categoryid);
        $.ajax({
            url: '{{ route('admin.products.getspecproducts')}}',
            type: 'get',
            data: {category_id:categoryid},
            dataType: 'json',
            beforeSend: function(request) {
                                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                            },
            success:function(data){
                // console.log(data);

                $("#sel_product").empty();
              var opt = $("#sel_product").append("<option value=''>Pick a product...</option>");
                for( var i = 0; i<data.length; i++){
                    opt.append("<option value='"+data[i].id+"'>"+data[i].title+"</option>");

                }
            }
        });
    });

    });

    $(document).ready(function(){
        $('#sel_product').change(function(){
            var productid = $(this).val();
            $.ajax({
               url: '{{ route('admin.products.getavailqty')}}',
               type: 'get',
               data: {product_id:productid},
               dataType: 'json',
               beforeSend: function(request) {
                                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                            },
                success:function(data){
                        // var r = data.length();
                    // console.log(r);
                    $("#avail").empty();
                    if(data==0)
                    document.getElementById('avail').value = 0;

                    else
                    document.getElementById('avail').value = data['avail_qty'];
                }
            });
        });
    });
    </script>

    @endsection