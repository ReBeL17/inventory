@extends('backend.layouts.user_app')
@section('content')


<div class="col-md-12">
    @if(isset ($model))
    {{ Form::model($model, ['route' => ['user.brands.update', $model], 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
    {{ Form::open(['route' => 'user.brands.store', 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">

                brands
            </h3>


        </div>
        <div class="box-body">
              

            <div class="form-group">
                {{ Form::label('name','Name', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('name')) has-error @endif ">
                    {{ Form::text('name', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the brand name']) }}
                    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->


        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('user.blogs', trans('cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div>
                <!--pull-left-->

                <div class="pull-right">
                    @if(isset($model))
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-info btn-xs']) }}
                    @else
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                    @endif
                </div>
                <!--pull-right-->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-image">
                <a href="{!! url('user/brands') !!}" class="btn btn-primary">add</a>
                brands
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>sn</th>
                            <th>Brand Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index = 0;?>
                        @foreach($data as $field)
                        <tr>
                            <td>{!! ++$index !!}</td>
                            <td>{!! $field->name !!}</td>
                            <td class="col-md-1">
                                {!! link_to_route('user.brands.edit', '||', array($field->id),
                                array('class' => 'fa fa-pencil-square-o fa-fw')) !!}
                                {!! link_to_route('user.brands.delete', '', array($field->id),
                                array('class' => 'fa fa-trash','onclick'=>"return confirm('Are you sure?')")) !!}
                            </td>

                            {!! Form::close() !!}


                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>




@stop



@section('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('pralu');
</script>
@endsection
