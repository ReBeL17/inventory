@extends('admin.backend.layouts.master')
@section('title','Suppliers Type')

@section('content')

@can('suppliertype-create')
   
<div class="col-md-12">
    @if(isset ($model))
    {{ Form::model($model, ['route' => ['admin.suppliers_type.update', $model], 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
    {{ Form::open(['route' => 'admin.suppliers_type.store', 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ (isset ($model)) ? 'Update Suppliers Type' : 'Add Suppliers Type' }}
            </h3>


        </div>
        <div class="box-body">

            <div class="form-group row">
                {{ Form::label('name','Type Name', ['class' => 'col-lg-2 control-label required']) }}

                <div class="col-lg-10  @if($errors->has('name')) has-error @endif ">
                    {{ Form::text('name', NULL, ['class' => 'form-control required', 'placeholder' =>'Enter the type name', 'required']) }}
                    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-right" style="margin: 0px 10px;">
                    {{ link_to_route('admin.suppliers_type', trans('Cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}

                    @if(isset($model))
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-info btn-xs']) }}
                    @else
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                    @endif
                </div>
                <!--pull-right-->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
 
@endcan

<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                List of Supplier Types
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-striped table-hover datatable datatable-type">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>SN</th>
                            <th>Supplier Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index = 0;?>
                        @foreach($data as $field)
                        <tr data-entry-id="{{ $field->id }}">
                            <td></td>
                            <td>{!! ++$index !!}</td>
                            <td>{!! $field->name !!}</td>         
                            
                            <td>
                                @can('suppliertype-edit')                                    
                                {!! link_to_route('admin.suppliers_type.edit','', array($field->id),
                                array('class' => 'fa fa-pencil-square-o fa-fw')) !!}                    
                                @endcan
                                @can('suppliertype-delete')  
                                {!! link_to_route('admin.suppliers_type.delete', '', array($field->id),
                                array('class' => 'fa fa-trash','onclick'=>"return confirm('Are you sure?')")) !!}
                                @endcan
                            </td>

                            {!! Form::close() !!}


                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>

@stop


@section('scripts')
    @parent
    <script>
        $(function () {
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    @can('suppliertype-delete')
      let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
      let deleteButton = {
        text: deleteButtonTrans,
        url: "{{ route('admin.suppliers_type.massDestroy') }}",
        className: 'btn-danger',
        action: function (e, dt, node, config) {
          var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
              return $(entry).data('entry-id')
          });
    
          if (ids.length === 0) {
            alert('{{ trans('global.datatables.zero_selected') }}')
    
            return
          }
    
          if (confirm('{{ trans('global.areYouSure') }}')) {
            $.ajax({
              headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
              method: 'POST',
              url: config.url,
              data: { ids: ids, _method: 'DELETE' }})
              .done(function () { location.reload() })
          }
        }
      }
      dtButtons.push(deleteButton)
    @endcan
    
      $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'desc' ]],
        pageLength: 100,
      });
      $('.datatable-type:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
    
    </script>
@endsection