@extends('admin.backend.layouts.master')
@section('title','Suppliers')

@section('content')
@can('suppliers-create')

<div class="col-md-12">
    @if(isset ($model))
    {{ Form::model($model, ['route' => ['admin.suppliers.update', $model], 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
    {{ Form::open(['route' => 'admin.suppliers.store', 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ (isset ($model)) ? 'Update Supplier' : 'Add Supplier' }}
            </h3>


        </div>
        <div class="box-body">

            <div class="form-group row">
                {{ Form::label('name','Supplier Name', ['class' => 'col-lg-2 control-label required']) }}

                <div class="col-lg-10  @if($errors->has('name')) has-error @endif ">
                    {{ Form::text('name', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the supplier name', 'required']) }}
                    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('type_id','Supplier Type', ['class' => 'col-lg-2 control-label required']) }}

                <div class="col-lg-10  @if($errors->has('type_id')) has-error @endif ">
                    {{ Form::select('type_id', $type, null, ['class' => 'form-control', 'placeholder' =>'Pick a supplier type...', 'required']) }}
                    @if ($errors->has('type_id')) <p class="help-block">{{ $errors->first('type_id') }}</p> @endif

                </div>
                <!--col-lg-10-->
                {{-- <div class="col-lg-1" style="cursor: pointer;">
                    <a class="btn btn-success btn-md" data-toggle='modal' data-target="#addPayment">
                            <i class="fa fa-plus"></i>
                    </a>
                </div> --}}
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('address','Address', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('address')) has-error @endif ">
                    {{ Form::text('address', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the address']) }}
                    @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('contact','Contact', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('contact')) has-error @endif ">
                    {{ Form::number('contact', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the contact']) }}
                    @if ($errors->has('contact')) <p class="help-block">{{ $errors->first('contact') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('status','Status', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('status')) has-error @endif ">
                    {{ Form::radio('status', 1, ['class' => 'form-control'])}} Active
                    {{ Form::radio('status', 0, ['class' => 'form-control']) }} Deactive
                    @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-right">
                    {{ link_to_route('admin.suppliers', trans('Cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                
                    @if(isset($model))
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-info btn-xs']) }}
                    @else
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                    @endif
                </div>
                <!--pull-right-->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

@endcan

<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                List of Suppliers
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-striped table-hover datatable datatable-supplier">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>SN</th>
                            <th>Supplier Name</th>
                            <th>Type</th>
                            <th>Address</th>
                            <th>Contact</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index = 0;?>
                        @foreach($data as $field)
                        <tr data-entry-id="{{ $field->id }}">
                            <td>

                            </td>
                            <td>{!! ++$index !!}</td>
                            <td>{!! $field->name !!}</td>
                            <td>{!! $field->suppliers_type->name !!}</td>
                            <td>{!! $field->address !!}</td>
                            <td>{!! $field->contact !!}</td>
                            
                                @if($field->status == 0 )
                                <td style="color:red">
                                Deactive
                                </td>
                                @else
                                <td style="color:green"> 
                                Active
                                </td>
                                @endif
                            
                                <td>
                                    @can('suppliers-edit')                                  
                                    {!! link_to_route('admin.suppliers.edit','', array($field->id),
                                    array('class' => 'fa fa-pencil-square-o fa-fw')) !!}                    
                                    @endcan
                                    @can('suppliers-delete') 
                                    {!! link_to_route('admin.suppliers.delete', '', array($field->id),
                                    array('class' => 'fa fa-trash','onclick'=>"return confirm('Are you sure?')")) !!}
                                    @endcan
                                </td>
                            
                            {!! Form::close() !!}


                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>


{{-- <div class="modal fade" id="addPayment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Supplier Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">    
                    <div class="box-body">      
                        <div class="form-group row">
                            {{ Form::label('name','Type Name', ['class' => 'col-lg-2 control-label']) }}
                            
                            <div class="col-lg-10  @if($errors->has('name')) has-error @endif ">
                               
                                {{ Form::text('name', NULL, ['id'=>'type', 'class' =>'form-control', 'placeholder' =>'Enter supplier type']) }}
                                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
            
                            </div>
                            <!--col-lg-10-->
                        </div>
                        <!--form control-->
                    </div>
                </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="addSupplierType">Ok</button>
            </div>
        </div>
       
    </div>

</div> --}}

@stop


@section('scripts')
    @parent
    <script>
    //     $(document).ready(function(){
    //     $('#addSupplierType').click(function(){
    //         var type = $('#type').val();
    //         // console.log(type);
    //         $.ajax({
    //            data:{data: type},
    //            url: 'suppliers_type/createtype',
    //            type: 'POST',
    //            beforeSend: function (request) {
    //                         return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    //                     },
    //             success: function (data){
    //                 location.reload();  
    //             }
    //         });
    //     });
    // }); 

        $(function () {
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    @can('suppliers-delete')
      let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
      let deleteButton = {
        text: deleteButtonTrans,
        url: "{{ route('admin.suppliers.massDestroy') }}",
        className: 'btn-danger',
        action: function (e, dt, node, config) {
          var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
              return $(entry).data('entry-id')
          });
    
          if (ids.length === 0) {
            alert('{{ trans('global.datatables.zero_selected') }}')
    
            return
          }
    
          if (confirm('{{ trans('global.areYouSure') }}')) {
            $.ajax({
              headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
              method: 'POST',
              url: config.url,
              data: { ids: ids, _method: 'DELETE' }})
              .done(function () { location.reload() })
          }
        }
      }
      dtButtons.push(deleteButton)
    @endcan
    
      $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'desc' ]],
        pageLength: 100,
      });
      $('.datatable-supplier:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
    
    </script>
@endsection