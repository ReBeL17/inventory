@extends('admin.backend.layouts.master')
@section('title','Category')
@section('content')

@can('category-create')

<div class="col-md-12">
    @if(isset ($model))
    {{ Form::model($model, ['route' => ['admin.categories.update', $model], 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'PATCH']) }}
    @else
    {{ Form::open(['route' => 'admin.categories.store', 'class' => 'form-horizontal', 'files'=> 'true', 'role' => 'form', 'method' => 'post']) }}
    @endif

    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ (isset ($model)) ? 'Update Category' : 'Add Category' }}
            </h3>


        </div>
        <div class="box-body">

            <div class="form-group row">
                {{ Form::label('title','Title', ['class' => 'col-lg-2 control-label required']) }}

                <div class="col-lg-10  @if($errors->has('title')) has-error @endif ">
                    {{ Form::text('title', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the category title']) }}
                    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

            <div class="form-group row">
                {{ Form::label('description','Description', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10  @if($errors->has('description')) has-error @endif ">
                    {{ Form::text('description', NULL, ['class' => 'form-control', 'placeholder' =>'Enter the category description']) }}
                    @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif

                </div>
                <!--col-lg-10-->
            </div>
            <!--form control-->

        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-right">
                    {{ link_to_route('admin.categories', trans('Cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                
                    @if(isset($model))
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-info btn-xs']) }}
                    @else
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                    @endif
                </div>
                <!--pull-right-->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
    
@endcan

<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                List of Categories
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-striped table-hover datatable datatable-category">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>SN</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index = 0;?>
                        @foreach($data as $field)
                        <tr data-entry-id="{{ $field->id }}">
                            <td></td>
                            <td>{!! ++$index !!}</td>
                            <td>{!! $field->title !!}</td>
                            <td>{!! $field->description !!}</td>
                            <td>
                                @can('category-edit')
                                {!! link_to_route('admin.categories.edit', '', array($field->id),
                                array('class' => 'fa fa-pencil-square-o fa-fw')) !!}
                                @endcan
                                @can('category-delete')
                                {!! link_to_route('admin.categories.delete', '', array($field->id),
                                array('class' => 'fa fa-trash','onclick'=>"return confirm('Are you sure?')")) !!}
                                @endcan
                            </td>

                            {!! Form::close() !!}


                        </tr>
                        @endforeach
                    </tbody>
                    {!!$data->render() !!}
                </table>
            </div>
        </div>
    </div>
</div>




@stop



{{-- @section('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('pralu');
</script>
@endsection --}}

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('category-delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.categories.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-category:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection