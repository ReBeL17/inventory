@extends('admin.backend.layouts.master')
@section('title','Receipts')


@section('content')

<div class="col-md-12">
    <div class="box box-custom">
        <div class="box-header with-border">
            <h3 class="box-image">
                {{ trans('cruds.receipt.title') }}
            </h3>


        </div>
        <div class="box-body">
            <div class="table-responsive">
       
            <table class="table table-bordered table-striped table-hover datatable datatable-receipt">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.receipt.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.receipt.fields.purchaseID') }}
                        </th>
                        
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($receipts as $key => $receipt)
                        <tr data-entry-id="{{ $receipt->id }}">
                            <td></td>
                            <td>
                                {{ $loop->index + 1 ?? '' }}
                            </td>
                            <td>
                                {{ $receipt->purchases->purchaseID ?? '' }}
                            </td>
                            
                            <td>
                                {{-- @can('receipt-show') --}}
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.receipts.generateReceipt', $receipt->id) }}">
                                        {{ trans('global.generate') }} Receipt
                                    </a>
                                {{-- @endcan --}}

                            </td>

                        </tr>
                    @endforeach
                </tbody>
                {!!$receipts->render() !!}
            </table>
        </div>
    </div>
</div>
</div>

@endsection


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
// @can('receipt-delete')
//   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
//   let deleteButton = {
//     text: deleteButtonTrans,
//     url: "{{ route('admin.purchases.massDestroy') }}",
//     className: 'btn-danger',
//     action: function (e, dt, node, config) {
//       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
//           return $(entry).data('entry-id')
//       });

//       if (ids.length === 0) {
//         alert('{{ trans('global.datatables.zero_selected') }}')

//         return
//       }

//       if (confirm('{{ trans('global.areYouSure') }}')) {
//         $.ajax({
//           headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
//           method: 'POST',
//           url: config.url,
//           data: { ids: ids, _method: 'DELETE' }})
//           .done(function () { location.reload() })
//       }
//     }
//   }
//   dtButtons.push(deleteButton)
// @endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-receipt:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection