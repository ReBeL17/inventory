@extends('admin.backend.layouts.master')
@section('title','Generate Receipt')

@section('content')

<div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h4>
          <i class="fas fa-globe"></i> Mango Soft Solution.
          <small class="float-right">Date: {{\Carbon\Carbon::now()->toDateString()}}</small>
        </h4>
      </div>
      <!-- /.col -->
    </div>
    <hr>
    <div class="row invoice-info" style="margin: 10px 0px 30px 0px;">
      <div class="col-sm-6 invoice-col">
      <strong>Receipt No. </strong>{{$receipt->purchases->purchaseID}} <br>
      <strong>Payment Date:</strong> {{$receipt->created_at->toDateString()}}<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <h5>Supplier Details :</h5>
    <div class="row invoice-info" style="border: 1px solid rgb(167, 167, 167);">
      <div class="col-sm-6 invoice-col">
        <strong>Name : </strong>{{$receipt->purchases->suppliers->name}} <br>
        <strong>Address : </strong>{{$receipt->purchases->suppliers->address}} <br>
      </div>
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">
        <strong>Contact : </strong>{{$receipt->purchases->suppliers->contact}} <br>
        {{-- <strong>Batch : </strong>{{$receipt->student->batch->name}} <br> --}}
      </div>
      <!-- /.col -->
     
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row" style="margin: 40px 15px 15px 15px;">
      <div class="col-12 table-responsive">
        <table class="table table-striped" style="margin-top:10px;">
          <thead>
              <tr>
                  <th>Particulars</th>
                  <th>Unit</th>
                  <th>Price</th>
                  <th>Amount</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$receipt->purchases->product_details}} </td>
              @if($receipt->paid === $receipt->purchases->total_price)
                <td>{{$receipt->purchases->total_qty}}</td>
                <td>{{$receipt->purchases->per_price}}</td>
              @else
                <td></td>
                <td></td>
              @endif
              <td>{{$receipt->paid}}</td>
            </tr>
            {{-- @foreach($receipt->fine_payments as $fine) --}}
            <tr>
              {{-- <td>{{$fine->fine_title}}</td>
              <td>{{$fine->unit}}</td>
              <td>{{$fine->price}}</td>
              <td>{{$fine->amount}}</td> --}}
            </tr>
            {{-- @endforeach --}}
          </tbody>
          <tfoot>
              <tr>
                  <td colspan="3" class="text-right"><b>Total:</b></td>
                  <td>{{$receipt->paid}}</td>
              </tr>
          </tfoot>
      </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    {{-- <div class="row">
      <label>Narration :</label>
    </div> --}}

    <div class="row">
      <div class="col-sm-9">
        <label>Remarks : </label>  
      {{ $receipt->remarks}}
      </div>
    </div>

    <div class="row">
      <div class="col-sm-9">
        <label>Created by : </label>  
      {{ auth()->user()->name}}
      </div>
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    @can('receipt-print')
      <div class="row no-print">
        <div class="col-12">
          <a href="{{route('admin.receipt-print', $receipt->id)}}" target="_blank" class="btn btn-default float-right"><i class="fas fa-print"></i> Print</a>
          
        </div>
      </div>
    @endcan
  </div>
  <!-- /.invoice -->
@endsection





