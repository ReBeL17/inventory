<?php


    return [

        /*
        |--------------------------------------------------------------------------
        | Menus Language Lines
        |--------------------------------------------------------------------------
        |
        | The following language lines are used in menu items throughout the system.
        | Regardless where it is placed, a menu item can be listed here so it is easily
        | found in a intuitive way.
        |
        */

          'frontend' => [
            'province'                   =>'Tribhuvan University',
            // 'ministry'                   =>'Provincial Government',
            // 'agri'=>'Department of Agriculture',
               'info'=>' Patan Multiple Campus ',
            'title3' =>'त्रिभुवन विश्वविद्यालय',
           
            'home'                       => 'Home' ,

            'about_us'                   => 'About Us' ,
            'about_ministry'                   => 'About Ministry' ,
            'citizen'                   => 'Citizen charter' ,
            'role'                   => 'Different Programs' ,
            'chart'                   => 'Organization chart' ,
            'who'                   => 'Staffs' ,
             'rule'                   => 'Law and Rule' ,
              'bill'     => 'Department Information',
            'notice'                   => 'Notice' ,

            'media_centre'                   => 'Media Center' ,
            'photo'                   => 'Photo Gallery' ,
            'audio'                   => 'Audio Gallery' ,
            'video'                   => 'Video Gallery' ,
            'readmore'                   => 'Read More ' ,

            'division'                   => 'Division/Section' ,

            'planning'                   => 'Administration Planning and Monitoring Division' ,
            'edu'                   => 'Education Division' ,
            'health'                   => 'Health Service Division' ,
            
            'new'  => 'Latest News' ,
             'roles'  => 'Functions Of Ministry' ,
            
            'link'     => 'Link' ,
            'login'     => 'Login' ,
            'contact'     => 'Contact ' ,
            'logout'  => 'Logout' ,
           'login'                      => 'Login' ,
            'admin'=>'Go To Admin Panel',


            
        ] ,



        



        'footer'=>[
            'contact'=>'Contact Details',
             'province'                   =>'Tribhuvan University ',
            'info'   => 'Patan Multiple Campus ',
            'title3' =>'Patan, Nepal',
           
            'phone'   => 'Phone ',
         

 'no'   => '061-467885',
            'fax'   => 'Fax ',
             'fno'=>' 01-5553791',
                'email'   => '    E-mail  ',
            'website'   => '    Website  ',
            'recent_update'   => '    Recent Update  ',
            'press'   => 'Press Release  ',

             'photo'                   => 'Photo ' ,
            'audio'                   => 'Audio ' ,
            'video'                   => 'Video ' ,
            'gallery'                 =>'Gallery',
             'view_more' =>'View More',
              'our' =>'Our',
             'publication' =>' Publication',
             'page' =>'Page Last Updated On ',
             'current' =>' Current Time ',
             'visitor' =>' Visitor Counter ',
             'stay' =>'Stay Connected ',
             'Toll_Free' =>'Toll Free No. ',
             'feedback' =>'Feedback',
'check_mail' =>'Check Mail',
'book' =>'Book ',
'link0' =>' Ministry of Agriculture and Livestock Development',
             'link1' =>' Nepal Agricultural Research Council ',
             'link2' =>'Ministry of Forests and Environment',
             'link3' =>'Project for Agriculture Commercialization and Trade',
             'link4' =>'Seed Quality Control Centre',



        ],


        'backend' => [
            'album'  => [
                'create' => 'Add New'
            ] ,
            'access' => [
                'title' => 'Access Management' ,

                'roles' => [
                    'all'        => 'All Roles' ,
                    'create'     => 'Create Role' ,
                    'edit'       => 'Edit Role' ,
                    'management' => 'Role Management' ,
                    'main'       => 'Roles' ,
                ] ,

                'users' => [
                    'all'             => 'All Users' ,
                    'change-password' => 'Change Password' ,
                    'create'          => 'Create User' ,
                    'deactivated'     => 'Deactivated Users' ,
                    'deleted'         => 'Deleted Users' ,
                    'edit'            => 'Edit User' ,
                    'main'            => 'Users' ,
                    'view'            => 'View User' ,
                ] ,
            ] ,

            'log-viewer' => [
                'main'      => 'Log Viewer' ,
                'dashboard' => 'Dashboard' ,
                'logs'      => 'Logs' ,
            ] ,

            'sidebar' => [
                'dashboard' => 'Dashboard' ,
                'purchase' => 'Purchases',
                'supplier' => 'Suppliers',
                'supplier_type' => 'Supplier Types',
                'customer' => 'Customers',
                'category' => 'Categories',
                'product' => 'Products',
                'payment' => 'Payments',
                'receive' => 'Collect Payment',
                'sale' => 'Sales',
                'receipt' => 'Receipts',
                'invoice' => 'Invoices',
                'general'   => 'General' ,
                'system'    => 'System' ,
                'photo'     => 'Photo' ,
                'album'     => 'Album' ,
                'image'     => 'Gallery' ,
                'folder'    => 'Folder' ,
                'setting'   => 'Setting',
            ] ,
        ] ,

        'language-picker' => [
            'language' => 'Language' ,
            /*
             * Add the new language to this array.
             * The key should have the same language code as the folder name.
             * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
             * Be sure to add the new language in alphabetical order.
             */
            'langs'    => [
               
                'en'    => 'English' ,
           
                'ne'    => 'नेपाली' ,
                
            ] ,
        ] ,
    ];
